<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Loadscreen</title>

<style>
#bar {
	display: inline-block;
	width: 80%;
	height: 4px;
	border-radius: 3px;
	-moz-border-radius: 3px;
	-webkit-border-radius: 3px;
	background: url('images/header-bg.png') repeat center top;
	background: RGBA(251,80,4,0.25);
}

#bar-width {height: 6px; border-radius: 3px; -moz-border-radius: 3px; -webkit-border-radius: 3px; background: RGBA(251,80,4,0.5);}
</style>
	
<script>
//Array randomizer (Fisher-Yates algorithm)
function shuffle(array) {
  var currentIndex = array.length, temporaryValue, randomIndex ;

  // While there remain elements to shuffle...
  while (0 !== currentIndex) {

    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;

    // And swap it with the current element.
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }

  return array;
}

function GameDetails( servername, serverurl, mapname, maxplayers, steamid, gamemode ) {
	setGamemode(gamemode);
	setMapname(mapname);

	if (!l_serverName && !l_serverImage) {
		setServerName(servername);
	}
}



function setGamemode(gamemode) {
	$("#gamemode").html(gamemode);
}

function setMapname(mapname) {
	$("#map").html(mapname);
}

function setServerName(servername) {
	$("#title").html(servername);
}

function setMusicName(name) {
	$("#music-name").fadeOut(2000, function() {
		$(this).html(name);
		$(this).fadeIn(2000);
	});
}

var youtubePlayer;
var actualMusic = -1;

$(function() {
	if (l_bgImagesRandom)
		l_bgImages = shuffle(l_bgImages);

	if (l_musicRandom)
		l_musicPlaylist = shuffle(l_musicPlaylist);

	if (l_messagesRandom)
		l_messages = shuffle(l_messages);

	if (l_messagesEnabled)
		showMessage(0);

	if (l_music) {
		loadYoutube();
		if (l_musicDisplay)
			$("#music").fadeIn(2000);
	}

	if (l_bgVideo) {
		$("body").append("<video loop autoplay muted><source src='"+l_background+"' type='video/webm'></video>");
	}else{
		$.backstretch(l_bgImages, {duration: l_bgImageDuration, fade: l_bgImageFadeVelocity});
	}

	if (l_serverName && !l_serverImage)
		setServerName(l_serverName);

	if (l_serverImage)
		setServerName("<img src='"+l_serverImage+"'>");

	if (l_bgOverlay)
		$("#overlay").css("background-image", "url('images/overlay.png')");

	$("#overlay").css("background-color", "rgba(0,0,0,"+(l_bgDarkening/100)+")");
});

function loadYoutube() {
	var tag = document.createElement('script');

	tag.src = "https://www.youtube.com/iframe_api";
	var firstScriptTag = document.getElementsByTagName('script')[0];
	firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
}

function onYouTubeIframeAPIReady() {
	youtubePlayer = new YT.Player('player', {
	  height: '390',
	  width: '640',
	  events: {
	    'onReady': onPlayerReady,
	    'onStateChange': onPlayerStateChange
	  }
	});
}

function onPlayerReady(event) {
	youtubePlayer.setVolume(l_musicVolume);
	if (youtubePlayer.isMuted()) youtubePlayer.unMute();
	nextMusic();
}

function onPlayerStateChange(event) {
    if (event.data == YT.PlayerState.ENDED) {
    	nextMusic();
	}
}

function nextMusic() {
	actualMusic++;

	if (actualMusic >= l_musicPlaylist.length) {
		actualMusic = 0;
	}

	var atual = l_musicPlaylist[actualMusic];

	if (atual.youtube) {
		youtubePlayer.loadVideoById(atual.youtube);
	}else{
		$("body").append('<audio src="'+atual.ogg+'" autoplay>');
		$("audio").prop('volume', l_musicVolume/100);
		$("audio").bind("ended", function() {
			$(this).remove();
			nextMusic();
		});
	}

	setMusicName(atual.name);
}

function showMessage(message) {
	if (message >= l_messages.length)
		message = 0;

	$("#messages").fadeOut(l_messagesFade, function() {
		$(this).html(l_messages[message]);
		$(this).fadeIn(l_messagesFade);
	});

	setTimeout(function() {
		showMessage(message+1);
	}, l_messagesDelay + l_messagesFade*2);
}
</script>

<style>
/* latin */
@font-face {
  font-family: 'Quicksand';
  font-style: normal;
  font-weight: 400;
  src: local('Quicksand Regular'), local('Quicksand-Regular'), url(http://fonts.gstatic.com/s/quicksand/v5/sKd0EMYPAh5PYCRKSryvW1tXRa8TVwTICgirnJhmVJw.woff2) format('woff2');
  unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2212, U+2215, U+E0FF, U+EFFD, U+F000;
}
/* latin */
@font-face {
  font-family: 'Quicksand';
  font-style: normal;
  font-weight: 700;
  src: local('Quicksand Bold'), local('Quicksand-Bold'), url(http://fonts.gstatic.com/s/quicksand/v5/32nyIRHyCu6iqEka_hbKsugdm0LZdjqr5-oayXSOefg.woff2) format('woff2');
  unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2212, U+2215, U+E0FF, U+EFFD, U+F000;
}
</style>

<style>
/* Youtube */
#player {
	position: absolute;
	left: -800px; top: -800px;
	z-index: -99;
}

body, html {
	padding: 0; margin: 0;
	width: 100%; height: 100%;
}

body {
	font-family: 'Quicksand';
	font-size: 16px;
	text-shadow: 1px 1px 0px rgb(0, 0, 0);
	color: rgb(255,255,255);
	background-color: black;
}

video {
	position: fixed; 
	right: 0; 
	bottom: 0;
	min-width: 100%; min-height: 100%;
	width: auto; height: auto;
	z-index: -2;
	background-size: cover;
	background-position: center; 
}

#title {
	font-size: 80px;
	margin-left: 12px;
}

#title img {
	margin-top: 18px;
}

#gamemode, #map {
	font-weight: bold;
}

#status-container {
	position: absolute;
	height: 50px;
	width: 100%;
	top: 50%;
	text-align: center;
	margin-top: -25px;
	font-size: 20px;
	line-height: 18px;
}

#loading-progress {
	height: 100%; width: 0%;
	background-color: white;
}

#messages {
	display: none;
	margin-top: 20px;
	font-size: 18px;
}

#subtitle {
	margin-left: 38px;
	margin-top: -10px;
}

#music {
	position: absolute;
	right: 10px; top: 0;
	margin: 12px;
	display: none;
	line-height: 24px;
}

#music-name {
	position: relative;
	font-weight: bold;
	left: 14px;
	margin-top: 6px;
}

#overlay {
	position: fixed;
	left: 0px; top: 0px;
	width: 100%; height: 100%;
	z-index: -1;
	background-repeat: repeat;
}
</style>
	<style>
	#block1 {
		position: absolute;
		left: 30px;
		bottom: 40px;
		border-radius: 5px;
		background-color: RGBA(251,80,4,0.7);
		width: 500px;
		height: 200px;
		box-shadow: 0px 0px 2px #FB5004;
	    transform: perspective( 900px ) rotateY( 28deg );
	}
#left11 {
	float: left;
}
#right11 {
	float: right;
	width: 250px;
    text-indent: 0.35em;
}

f2 {
    font-size: 1.3em;
    font-weight: bold;
	display: block;
	text-align: -webkit-center;
}
f3 {
    font-size: 1.17em;
    font-weight: bold;
	display: block;
}
.vertical-line{
  height: 135px;
  float: left; 
  border: 1px inset;
}
.horizontal-line{
  border: 1px inset;
}
</style>
</head>
<body>

		<div id="block1">
		<img src="Untitled.png"><f2>Uppdatering 1.0</f2>
				<div class="horizontal-line"></div>
		<div id="right11">
				<div class="vertical-line"></div>
		<f3>● Tack för att du joinar!</f3>
		

		</div>
		
		

		<div id="left11">
		<f3>● Inga nyheter än!</f3>
		

		

		</div>
		
		</div>



	<div id="player"></div>
	<div id="overlay"></div>

	<div id="title">Loading</div>
	<div id="subtitle">
		Sensate uppdateringen <span id="gamemode">1.0</span> kom <span id="map">idag</span>
	</div>

	<div id="status-container">

		<div id="messages"></div>
        <div id="bar">
        	<div id="bar-width" style="width: 0%;"></div>
       	</div>

        <div id="percentage"><p></p></div><div id="download-item"><p>Laddar ner spelinfo...</p></div>
	</div>

	<div id="music">Just nu spelar vi upp<div id="music-name"></div></div>

	<script src="js/lib/jquery-2.1.1.min.js"></script>
	<script src="js/lib/jquery.backstretch.min.js"></script>
	<script src="js/config.js"></script>
	<script src="js/sleek.js"></script>
	<script src="scripts/cycle.js"></script>
	<script src="scripts/main.js"></script>
</body>
</html>