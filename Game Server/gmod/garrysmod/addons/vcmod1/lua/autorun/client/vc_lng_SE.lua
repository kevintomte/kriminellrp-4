// Copyright © 2012-2015 VCMod (freemmaann). All Rights Reserved. if you have any complaints or ideas contact me: email - freemmaann@gmail.com or skype - comman6.
local Lng = {}

//You do not have to translate everything. Things that are not translated will be written in English.

//Set up the language
Lng.Name = "Svenska"
Lng.Language_Code = "SE"
Lng.Translated_By_Name = "System"
Lng.Translated_By_Link = "http://steamcommunity.com//id/systemhacks/"
Lng.Translated_Date = "2015 04 01"

////////////// Translation starts here

////////GENERAL
Lng.Locked = "Låst."
Lng.UnLocked = "Upplåst."
Lng.Chat = 'För att justera inställningar (lampor, vy) skriv "!vcmod" i chatten.'
Lng.Broken = "Den här bilen är trasig och måste repareras."
Lng.Trl_Atch = "Släp påkopplat."
Lng.Trl_Detch = "Släp frånkopplat."
Lng.ELS_TuningIntoPoliceC = "Justering i polis radion"
Lng.ELS_NoPoliceRCFound = "Ingen polis radio hittad."

////////Pickup
Lng.TouchCar100 = "Vidrör en bil för att reparera 100%."
Lng.TouchCar25 = "Vidrör en bil för att reparera +25% av standard livet."
Lng.TouchCar10 = "Vidrör en bil för att reparera +10% av standard livet."

////////MENU general
Lng.Info = "Info"
Lng.Menu = "Meny"
Lng.Language = "Språk"
Lng.Personal = "Personligt"
Lng.Administrator = "Administratör"
Lng.Options = "Inställningar"
Lng.ELSOptions = "ELS Inställningar"
Lng.Main = "Main"
Lng.Controls = "Kontroller"
Lng.HUD = "HUD"
Lng.View = "Vy"
Lng.Radio = "Radio"
Lng.Multiplier = "Gånger"
Lng.OptOnly_You = "Dessa inställningar är personliga"
Lng.OptOnly_Admin = "Dessa inställningar kan bara ändras av en administratör"
Lng.NPC_Settings = "NPC Inställningar"
Lng.Height = "Höjd"
Lng.FadeOutDistance = "Uttoning avstånd"
Lng.Reset = "Återställ"
Lng.Save = "Spara"
Lng.Load = "Ladda"
Lng.Volume = "Volym"
Lng.Distance = "Distans"
Lng.None = "None"
Lng.EnterKey = "Enter Knapp"

Lng.Enabled_Cl = "Tillåt clientside"
Lng.Enabled_Sv = "Tillåt serverside"

Lng.Lights = "Lampor"
Lng.Health = "Liv"
Lng.Sound = "Ljud"
Lng.Other = "Annat"
Lng.CreatedBy = "Skapad av"
Lng.Enabled = "Aktiverad"
Lng.OffTime = "Off time"
Lng.Time = "Tid"
Lng.DistMultiplier = "Gångra Distansen"
Lng.ControlsReset = "Återställ kontroller."
Lng.SettingsSaved = "Inställningar sparade."
Lng.SettingsReset = "Inställningar återställda."
Lng.LoadedSettingsFromServer = "Inställningar laddade från servern."
Lng.InteriorIndicators = "Interiör indikatorer"
Lng.ExtraGlow = "Extra Lyseffekt"

////////MENU ELS
Lng.ManulSiren = "Manual siren"
Lng.ELS_SirenSwitch = "ELS siren switch"
Lng.ELS_SirenToggle = "ELS siren av/på"
Lng.ELS_LightsSwitch = "ELS lampor switch"
Lng.ELS_LightsToggle = "ELS lights av/på"
Lng.VCModMainEnabled = "VCMod main aktiverad"
Lng.VCModELSEnabled = "VCMod ELS aktiverad"
Lng.ELSLightsEnabled = "ELS lampor aktiverad"
Lng.AutoDisableELSLights = "Automatiskt avaktivera ELS lights"
Lng.OffOnExit = "Avaktivera när du kliver ur bilen"
Lng.Siren = "Siren"
Lng.AutoDisableELSSounds = "Automatiskt avaktivera ELS ljud"
Lng.Manual = "Manual"
Lng.Bullhorn = "Tuta"
Lng.PoliceChatterEnabled = "Polis radio aktiverad"
Lng.PoliceChatter = "Polis radio"
Lng.PoliceChatter_Info = "Polis radion är i realtid med en officiel polis radio"
Lng.SelectedRadioChatter = "Välj radio frekvens"
Lng.ReduceDamageToEmergencyVehicles = "Redusera skadan för utryckningsfordon"

////////MENU personal info
Lng.YouAreUsingVCMod = "Du använderVCMod"
Lng.ServerIsUsingVCMod = "Denna server använder VCMod"
Lng.Info_EverThought = "Någonsin känt att Gmods fordon inte är realistiska eller att de saknas saker från dem ? \nVCMod är utformat för att göra Garrysmods fordon lika bra som i alla andra spel ."
Lng.Info_VCModHasFollowingAddons = "VCMod has the following addons:"

////////MENU personal options
Lng.VisDist = "Distans av vy"
Lng.Warmth = "Värme"
Lng.Lines = "Linjer"
Lng.Glow = "Sken"
Lng.DynamicLights = "Dynamiska lampor"
Lng.ThirdPView = "Tredjepersonsvy"
Lng.DynamicView = "Dynamisk vy"
Lng.AutoFocus = "Auto fokus"
Lng.Reverse = "Backen"
Lng.VectorStiffness = "Vektor styvnad %"
Lng.AngleStiffness = "Vinkel styvnad %"
Lng.IgnoreWorld = "Ignora världen"
Lng.TruckView = "Lastbils vy"

////////MENU controls
Lng.HoldDuration = "Knapphållning"
Lng.Mouse = "Mus"
Lng.KeyboardInput = "Tangenbordsknapp"
Lng.MouseInput = "Mus knapp"

Lng.NightLights = "Natt lampor"
Lng.HeadLights = "Strålkastare"
Lng.LowHigh = "Låg/Hög stråle toggle"
Lng.HazardLights = "Varningsblinkers"
Lng.BlinkerLeft = "Vänstra blinkern"
Lng.BlinkerRight = "Högra blinkern"
Lng.Horn = "Tuta"
Lng.Cruise = "Cruise"
Lng.LockUnlock = "Lås/Låsupp"
Lng.LookBehind = "Titta bakåt"
Lng.DetachTrl = "Frånkoppla släp"

////////MENU hud
Lng.Effect3D = "3D effekt"
Lng.HUDHeight = "Sido HUD höjd %"
Lng.HUD_Name = "Namn"
Lng.HUD_Icons = "Ikoner"
Lng.HUD_Cruise = "Cruise"
Lng.HUD_Cruise_MPH = "mi/h istället för km/h"
Lng.HUD_Repair = "Reparera"
Lng.HUD_ELS_Siren = "ELS siren"
Lng.HUD_ELS_Lights = "ELS lampor"

////////MENU admin options
Lng.HandbrakeLights = "Handbroms lampor"
Lng.InteriorLights = "Inrednings lampor"
Lng.BlinkersOffExit = "Blinkers stängs av när du kliver ur bilen"

Lng.DamageEnabled = "Skada aktiverad"
Lng.StartHealthMultiplier = "Gångra start livet"
Lng.PhysicalDamage = "Fysisk skada"
Lng.FireDuration = "Tid på eld"
Lng.RemoveCarAfterExplosion = "Ta bort bilen efter explosion"
Lng.ReducePlayerDmgWhileInCar = "Redusera spelarens skada i ett fordon"

Lng.DoorSounds = "Dörr ljud"
Lng.TruckRevBeep = "Lastbilens backnings ljud"

Lng.SteeringWheelLOnExit = "Rattlås när du kliver ur bilen"
Lng.BrakesLOnExit = "Bromsarna låser sig när du kliver ur bilen"
Lng.WheelDust = "Däck rök"
Lng.WheelDustWhileBraking = "Däck rök när du bromsar"
Lng.MatchPlayerSpeedExit = "Matcha spelarens hastighet när man kliver ur bilen"
Lng.NoCollidePlyOnExit = "Ta bort kollision från spelaren när denne kliver ur bilen"
Lng.Exhaust = "Avgasrör"
Lng.PassengerSeats = "passagerar säte"
Lng.TrailerAttach = "Släp påkopplat"
Lng.TrailerAttachConStrengthM = "Gångra styrkan i de påkopplade släpet"
Lng.TrailersCanAtchToReg = "Släp kan kopplas på vanliga bilar"
Lng.RepairToolSpeedMult = "Gångrad hastighet för reparerings verktyget"

if !VC_Lng_T then VC_Lng_T = {} end VC_Lng_T[Lng.Language_Code] = Lng