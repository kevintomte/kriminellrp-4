// Copyright © 2012-2015 VCMod (freemmaann). All Rights Reserved. if you have any complaints or ideas contact me: email - freemmaann@gmail.com or skype - comman6.
local Lng = {}

//You do not have to translate everything. Things that are not translated will be written in English.

//Set up the language
Lng.Name = "Polski"
Lng.Language_Code = "PL"
Lng.Translated_By_Name = "CryptAlchemy"
Lng.Translated_By_Link = "http://steamcommunity.com//id/cryptalchemy/"
Lng.Translated_Date = "2015 03 16"

////////////// Translation starts here

////////GENERAL
Lng.Locked = "Pojazd zamknięty."
Lng.UnLocked = "Pojazd otwarty."
Lng.Chat = 'Żeby zmienić opcje (światła samochodu, kierunek widzenia) wpisz "!vcmod" w chat.'
Lng.Broken = "Ten samochód jest zepsuty, musi być naprawiony."
Lng.Trl_Atch = "Przyczepa połączona."
Lng.Trl_Detch = "Przyczepa odłączona."
Lng.ELS_TuningIntoPoliceC = "Słuchasz rozmowe policji"
Lng.ELS_NoPoliceRCFound = "Nie znaleziono stacji radiowej policji."

////////Pickup
Lng.TouchCar100 = "Dotknij samochód żeby naprawić do 100%."
Lng.TouchCar25 = "Dotknij samochód żeby naprawić +25% oryginalnego zdrowia."
Lng.TouchCar10 = "Dotknij samochód żeby naprawić +10% oryginalnego zdrowia."

////////MENU general
Lng.Info = "Informacje"
Lng.Menu = "Menu"
Lng.Language = "Język"
Lng.Personal = "Osobisty"
Lng.Administrator = "Administrator"
Lng.Options = "Opcje"
Lng.ELSOptions = "ELS Opcje"
Lng.Main = "Główne"
Lng.Controls = "Klawisze"
Lng.HUD = "HUD"
Lng.View = "Kierunek Widzenia"
Lng.Radio = "Radio"
Lng.Multiplier = "Mnożnik"
Lng.OptOnly_You = "Te opcje będą miały efekt na ciebie"
Lng.OptOnly_Admin = "Te opcje tylko mogą być zmienione przez administratora"
Lng.NPC_Settings = "Ustawienia NPC"
Lng.Height = "Wysokość"
Lng.FadeOutDistance = "Odległość zanikania"
Lng.Reset = "Zmarz"
Lng.Save = "Zarejestruj"
Lng.Load = "Wprowadź"
Lng.Volume = "Dźwięk"
Lng.Distance = "Odległość"
Lng.None = "Żadne"
Lng.EnterKey = "Wpisz klawisz"

Lng.Enabled_Cl = "Włącz po stronie klienta"
Lng.Enabled_Sv = "Włącz po stronie serwera"

Lng.Lights = "Światła"
Lng.Health = "Zdrowie"
Lng.Sound = "Dźwięk"
Lng.Other = "Inne"
Lng.CreatedBy = "Zrobione przez"
Lng.Enabled = "Włączone"
Lng.OffTime = "Czas wyłączenia"
Lng.Time = "Czas"
Lng.DistMultiplier = "Mnożnik odległośći"
Lng.ControlsReset = "Klawisze skasowane."
Lng.SettingsSaved = "Ustawienia zarejestrowane."
Lng.SettingsReset = "Ustawienia skasowane."
Lng.LoadedSettingsFromServer = "Ustawienia wprowadzone z serwera."
Lng.InteriorIndicators = "Wewnętrzne wskaźniki"
Lng.ExtraGlow = "Dodatkowe oświetlenie"

////////MENU ELS
Lng.ManulSiren = "Manualna syrena"
Lng.ELS_SirenSwitch = "Zmień syrenę ELS"
Lng.ELS_SirenToggle = "Przełącz syrenę ELS"
Lng.ELS_LightsSwitch = "Zmień światła ELS"
Lng.ELS_LightsToggle = "Przełącz światła ELS"
Lng.VCModMainEnabled = "VCMod główny włączony"
Lng.VCModELSEnabled = "VCMod ELS włączony"
Lng.ELSLightsEnabled = "Światła ELS włączone"
Lng.AutoDisableELSLights = "Automatycznie wyłącz światła ELS"
Lng.OffOnExit = "wyłączone na wyjściu"
Lng.Siren = "Syrena"
Lng.AutoDisableELSSounds = "Automatycznie wyłącz dźwięki ELS"
Lng.Manual = "Manualna"
Lng.Bullhorn = "Ciężki sygnał"
Lng.PoliceChatterEnabled = "Policijna rozmowa wyłączona"
Lng.PoliceChatter = "Policijna rozmowa"
Lng.PoliceChatter_Info = "Policijna rozmowa jest transmisją oficjalną przez radio."
Lng.SelectedRadioChatter = "Wybrana stacja policyjnej rozmowy"
Lng.ReduceDamageToEmergencyVehicles = "Zmniejsz niszczenie samochodów awaryjnych"

////////MENU personal info
Lng.YouAreUsingVCMod = "Używasz VCMod"
Lng.ServerIsUsingVCMod = "Ten serwer używa VCMod"
Lng.Info_EverThought = "Czy kiedykolwiek myślałeś że samochody w gmod nie są wystarczająco realistyczne albo im coś brakuje?\nVCMod jest zrobiony po to żeby zrobić samochody w gmod tak realistyczne jak w innych grach."
Lng.Info_VCModHasFollowingAddons = "VCMod ma następujące funkcje:"

////////MENU personal options
Lng.VisDist = "Odległość widoczności"
Lng.Warmth = "Ciepłe światła"
Lng.Lines = "Linie"
Lng.Glow = "Jasność"
Lng.DynamicLights = "Dynamiczne światła"
Lng.ThirdPView = "Widok z trzeciej osoby"
Lng.DynamicView = "Dynamiczny widok"
Lng.AutoFocus = "Automatyczną ostrość"
Lng.Reverse = "Cofanie"
Lng.VectorStiffness = "Sztywność wektorów %"
Lng.AngleStiffness = "Sztywność kątów %"
Lng.IgnoreWorld = "Ignorowanie świata"
Lng.TruckView = "Widok połączonego traka"

////////MENU controls
Lng.HoldDuration = "Czas trzymania"
Lng.Mouse = "Mysz"
Lng.KeyboardInput = "Pisanie klawiaturą"
Lng.MouseInput = "Pisanie myszą"

Lng.NightLights = "Światła nocne"
Lng.HeadLights = "Światła samochodu"
Lng.LowHigh = "Ustawienia bliskich i długich świateł"
Lng.HazardLights = "Światła awaryjne"
Lng.BlinkerLeft = "Lewy migacz"
Lng.BlinkerRight = "Prawy migacz"
Lng.Horn = "Sygnał"
Lng.Cruise = "Tempomat"
Lng.LockUnlock = "Zamknij/Otwórz"
Lng.LookBehind = "Patrz do tyłu"
Lng.DetachTrl = "Odłącz przyczepe"

////////MENU hud
Lng.Effect3D = "Efekt 3D"
Lng.HUDHeight = "Wysokość boczna HUD %"
Lng.HUD_Name = "Imię"
Lng.HUD_Icons = "Ikony"
Lng.HUD_Cruise = "Tempomat"
Lng.HUD_Cruise_MPH = "mi/h zamiast km/h"
Lng.HUD_Repair = "Napraw"
Lng.HUD_ELS_Siren = "Syrena ELS"
Lng.HUD_ELS_Lights = "Światła ELS"

////////MENU admin options
Lng.HandbrakeLights = "Światła hamulca ręcznego"
Lng.InteriorLights = "Światła wewnętrzne"
Lng.BlinkersOffExit = "Migacze (sygnały skręcania) wyłączone na wyjściu"

Lng.DamageEnabled = "Uszkodzenie włączone"
Lng.StartHealthMultiplier = "Mnożnik początkowego zdrowia"
Lng.PhysicalDamage = "Uszkodzenie fizyczne"
Lng.FireDuration = "Czas trwania ognia"
Lng.RemoveCarAfterExplosion = "Skasuj samochód po wybuchu"
Lng.ReducePlayerDmgWhileInCar = "Zmniejsz uszkodzenie gracza w samochodzie"

Lng.DoorSounds = "Dźwięki drzwi"
Lng.TruckRevBeep = "Dźwięki cofania traka"

Lng.SteeringWheelLOnExit = "Kierownica zablokowana na wyjściu"
Lng.BrakesLOnExit = "Hamulce zablokowane na wyjściu"
Lng.WheelDust = "Pył opon"
Lng.WheelDustWhileBraking = "Pył opon w czasie hamowania"
Lng.MatchPlayerSpeedExit = "Dopasuj prędkość gracza z samochodem na wyjściu"
Lng.NoCollidePlyOnExit = "Nie koliduj gracza z samochodem na wyjściu"
Lng.Exhaust = "Spaliny"
Lng.PassengerSeats = "Siedzenia pasażerów"
Lng.TrailerAttach = "Połącz przyczepe"
Lng.TrailerAttachConStrengthM = "Mnożnik siły połączenia przyczepy"
Lng.TrailersCanAtchToReg = "Przyczepa może się przyczepić do normalnych samochodów"
Lng.RepairToolSpeedMult = "Mnożnik szybkości urządzenia do naprawiania samochodów"

if !VC_Lng_T then VC_Lng_T = {} end VC_Lng_T[Lng.Language_Code] = Lng