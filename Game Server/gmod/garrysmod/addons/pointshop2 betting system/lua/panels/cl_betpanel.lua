/*---------------------------------------------------------
Bet Panel
---------------------------------------------------------*/
local BetPanel = {}

function BetPanel:Init()
	self:SetDrawBackground(false)
	//self:SetDrawBorder(false)
	//self:SetStretchToFit(false)
	self:SetSize(250, 400)
	self.PercentToWidth = 0
	self.VotePercentage = 0
	self.AvatarSize = 32
	self.PrimaryOdds = 0
	self.SecondaryOdds = 0
	self.SecondaryBarWidth = 5
	self.BackColor = BETTING.Theme.ControlColor
	self.FlashColor = self.BackColor
	self.TextColor = Color(255, 255, 255, 150)
	self.PrimaryBarColor = Color(0, 204, 68)
	self.SecondaryBarColor = Color(255, 77, 77)
	self.WinningColor = Color(0, 204, 68)
	self.LosingColor = Color(255, 77, 77)
	self.Hovering = false
	
	self.HeaderLbl = vgui.Create("DLabel", self)
	self.HeaderLbl:SetFont("Bebas40Font")
	
	self.DescriptionLbl = vgui.Create("DLabel", self)
	self.DescriptionLbl:SetFont("OpenSans18Font")
	
	self.OddsLbl = vgui.Create("DLabel", self)
	self.OddsLbl:SetFont("Bebas120Font")
	
	self.SecondaryLbl = vgui.Create("DLabel", self)
	self.SecondaryLbl:SetFont("OpenSans20Font")
	
	self.SecondaryBtn = vgui.Create("DImageButton",self)
	self.SecondaryBtn.OnCursorEntered = function() self.SecondaryBarWidth = 5 self.SecondaryHovering = true end
	self.SecondaryBtn.OnCursorExited = function() self.SecondaryHovering = false end
	self.SecondaryBtn:SetDrawBackground(false)
	self.SecondaryBtn:SetDrawBorder(false)

	self.TotalBetsLbl = vgui.Create("DLabel", self)
	self.TotalBetsLbl:SetFont("OpenSans35Font")
	
end

function BetPanel:SetData(primary,secondary,multiplier,betonothers)
	self.PrimaryTeam = primary
	self.SecondaryTeam = secondary
	self.Multiplier = multiplier
	
	if secondary then
		self.HeaderLbl:SetText(string.format("Bet on %s",BETTING.GetTeamName(primary)))
		self.HeaderLbl:SizeToContents()
		self.PrimaryBarColor = BETTING.GetTeamColor(primary)
		
		self.DescriptionLbl:SetText(string.format("Place a bet on %s\nCurrent bet multiplier is: x%i",
		BETTING.GetTeamName(primary),multiplier))
		self.DescriptionLbl:SizeToContents()
		
		
		self.SecondaryLbl:SetText(string.format("Place a bet on %s instead",BETTING.GetTeamName(secondary)))
		self.SecondaryLbl:SizeToContents()
		self.SecondaryBarColor = BETTING.GetTeamColor(secondary)
	else
		self.HeaderLbl:SetText(betonothers and "Bet on any player" or "Bet on yourself")
		self.HeaderLbl:SizeToContents()
		self.PrimaryBarColor = BETTING.GetTeamColor(primary)
		
		self.DescriptionLbl:SetText(string.format("%s.\nCurrent bet multiplier is: x%i",
		betonothers and "Whoever you bet on must stay alive and win the round" or "You must stay alive and win the round", multiplier))
		self.DescriptionLbl:SizeToContents()
	
		self.SecondaryLbl:Hide()
		self.SecondaryBtn:Hide()
	end

	
	self.OddsLbl:SetText(string.format("x%i",multiplier))
	self.OddsLbl:SizeToContents()
end

function BetPanel:SetTotalBets(total,pointsname)
	self.TotalBetsLbl:SetText(string.format("Total Bets: %s %s",total,pointsname))
	self.TotalBetsLbl:SizeToContents()
end

function BetPanel:GetPrimaryTeam()
	return self.PrimaryTeam
end

function BetPanel:GetSecondaryTeam()
	return self.SecondaryTeam
end

function BetPanel:SetActiveBet(beton, amount, plybeton)
	if not (beton == BETTING.GetTeamID(BETTING.FirstTeam)) and not (beton == BETTING.GetTeamID(BETTING.SecondTeam)) then return end
	if BETTING.Settings.AllowBetsOnOthers and not IsValid(plybeton) then return
	elseif IsValid(plybeton) then self.PlayerBetOn = plybeton end
	self.ActiveBet = true
	if self.SecondaryTeam  then
		self.HeaderLbl:SetText(string.format("Bet placed on %s",BETTING.GetTeamName(beton)))
		self.HeaderLbl:SizeToContents()
		self.PrimaryBarColor = BETTING.GetTeamColor(beton)
		
		self.DescriptionLbl:SetText(string.format("You have successfully placed a bet on %s\nThe current multiplier is x%i, you stand to win:",
		BETTING.GetTeamName(beton),self.Multiplier))
		self.DescriptionLbl:SizeToContents()
	else
		self.HeaderLbl:SetText(self.PlayerBetOn and !(self.PlayerBetOn == LocalPlayer()) and self.PlayerBetOn:Nick() or "Bet placed on yourself")
		self.HeaderLbl:SizeToContents()
		self.PrimaryBarColor = BETTING.GetTeamColor(self.PlayerBetOn and BETTING.GetPlayerTeam(LocalPlayer()) or beton)
		
		self.DescriptionLbl:SetText(string.format("%s.\nThe current multiplier is x%i, you stand to win:",
		self.PlayerBetOn and !(self.PlayerBetOn == LocalPlayer()) and "They must win the round and stay alive" or "Win the round and stay alive",self.Multiplier))
		self.DescriptionLbl:SizeToContents()
	end
	
	self.OddsLbl:SetVisible(true)
	self.OddsLbl:SetFont("Bebas40Font")
	self.OddsLbl:SetText(string.format("%s %s",BETTING.FormatNumber(amount * BETTING.Settings.BetMultiplier),BETTING.GetPointsName()))
	self.OddsLbl:SizeToContents()
	
	self.SecondaryLbl:Hide()
	self.SecondaryBtn:Hide()
	
	self:SetFlash(BETTING.GetTeamColor(self.PlayerBetOn and BETTING.GetPlayerTeam(LocalPlayer()) or beton))
end

function BetPanel:SetWinningBet(beton, winnings)
	if not (beton == BETTING.GetTeamID(BETTING.FirstTeam)) and not (beton == BETTING.GetTeamID(BETTING.SecondTeam)) then return end
	
	self.ActiveBet = true
	self.BetWon = true
	self.HeaderLbl:SetText("You Win")
	self.HeaderLbl:SizeToContents()
	self.PrimaryBarColor = self.WinningColor
	
	if self.SecondaryTeam then
		self.DescriptionLbl:SetText(string.format("Your team (%s) has won the round!\nYou have recieved:",
		BETTING.GetTeamName(beton)))
		self.DescriptionLbl:SizeToContents()
	else
		self.DescriptionLbl:SetText(string.format("%s won the round and stayed alive!\nYou have recieved:",
		self.PlayerBetOn and !(self.PlayerBetOn == LocalPlayer()) and "They" or "You"))
		self.DescriptionLbl:SizeToContents()
	end
	
	self.OddsLbl:SetVisible(true)
	self.OddsLbl:SetFont("Bebas40Font")
	self.OddsLbl:SetText(string.format("+%s %s",BETTING.FormatNumber(winnings),BETTING.GetPointsName()))
	self.OddsLbl:SizeToContents()
	
	self.SecondaryLbl:Hide()
	self.SecondaryBtn:Hide()
	
	self:SetFlash(self.WinningColor)
end

function BetPanel:SetLosingBet(beton, amount)
	if not (beton == BETTING.GetTeamID(BETTING.FirstTeam)) and not (beton == BETTING.GetTeamID(BETTING.SecondTeam)) then return end
	
	self.ActiveBet = true
	self.BetLost = true
	self.HeaderLbl:SetText("You Lose")
	self.HeaderLbl:SizeToContents()
	self.PrimaryBarColor = self.LosingColor
	
	if self.SecondaryTeam then
		self.DescriptionLbl:SetText(string.format("Your team (%s) has lost.\nYou lose your bet:",
		BETTING.GetTeamName(beton)))
		self.DescriptionLbl:SizeToContents()
	else
		local alive 
		if self.PlayerBetOn then if IsValid(self.PlayerBetOn) and self.PlayerBetOn:Alive() then alive = true end
		else alive = LocalPlayer():Alive() end
		self.DescriptionLbl:SetText(string.format("%s %s.\nYou lose your bet:",
		self.PlayerBetOn and !(self.PlayerBetOn == LocalPlayer()) and "They" or "You",alive and "lost the round" or "died that round" ))
		self.DescriptionLbl:SizeToContents()
	end
	
	self.OddsLbl:SetVisible(true)
	self.OddsLbl:SetFont("Bebas40Font")
	self.OddsLbl:SetText(string.format("-%s %s",BETTING.FormatNumber(amount),BETTING.GetPointsName()))
	self.OddsLbl:SizeToContents()
	
	self.SecondaryLbl:Hide()
	self.SecondaryBtn:Hide()
	
	self:SetFlash(self.LosingColor)
end

function BetPanel:SetFKeyMessage(key, timeout)
	self.FKeyLbl = vgui.Create("DLabel", self)
	self.FKeyLbl:SetFont("OpenSans20Font")
	//self.FKeyLbl:SetTextColor(self.PrimaryBarColor)
	self.FKeyLbl:SetText(string.format("Press %s to show the cursor.",key))
	self.FKeyLbl:SizeToContents()
	self.FKeyLbl:AlphaTo(0,0.5,timeout or 5, function() self.FKeyLbl:Remove() end)
end

function BetPanel:SetTimer(seconds)
	self.Timer = seconds
	self.EndTime = CurTime() + seconds 
end

function BetPanel:SetFlash(color)
	self.FlashColor = color
	self.CurrentFlashAlpha = 255
end

function BetPanel:SetHiding(hiding)
	self.IsHiding = hiding
	if hiding then self:AlphaTo(0,0.5,0)
	else self:AlphaTo(200,1.5,0) end
end

function BetPanel:GetHiding()
	return self.IsHiding
end

function BetPanel:GetActiveBet()
	return self.ActiveBet
end

function BetPanel:IsShowingBetResult()
	return self.BetWon or self.BetLost
end

function BetPanel:PerformLayout()
	local offset = (self:GetWide() - self.HeaderLbl:GetWide()) / 2
	self.HeaderLbl:SetPos(offset, 10)
	
	self.DescriptionLbl:SetPos(5, 60)
	
	local oddsoffset = (self:GetWide() - self.OddsLbl:GetWide()) / 2
	self.OddsLbl:SetPos(oddsoffset, 100)
	
	self.SecondaryLbl:SetPos(10, self:GetTall() - 25)
	self.SecondaryBtn:SetPos(0,self:GetTall() - 30)
	self.SecondaryBtn:SetSize(self:GetWide(), 30)
	
	if IsValid(self.FKeyLbl) then
		local fkeyoffset = (self:GetWide() - self.FKeyLbl:GetWide()) / 2
		self.FKeyLbl:SetPos(fkeyoffset, self:GetTall() - 60 - self.TotalBetsLbl:GetTall() )
	end
	
	local betsoffset = (self:GetWide() - self.TotalBetsLbl:GetWide()) / 2
	self.TotalBetsLbl:SetPos(betsoffset, self:GetTall() - 80)
end

function BetPanel:Paint()
	surface.SetDrawColor(self.BackColor)
	surface.DrawRect( 0, 0, self:GetWide(), self:GetTall() - 35)
	
	//Primary bet bar
	surface.SetDrawColor(self.PrimaryBarColor)
	surface.DrawRect( 0, 0, self:GetWide(), 50)
	
	//Secondary bet bar
	if not self.ActiveBet and self.SecondaryTeam then
		surface.SetDrawColor(self.BackColor)
		surface.DrawRect( 0, self:GetTall() - 30, self:GetWide(), 30)
		//Bar flick
		self.SecondaryBarWidth = math.Approach(self.SecondaryBarWidth, self.SecondaryHovering and self:GetWide() or 5, FrameTime() * 800 )
		
		surface.SetDrawColor(self.SecondaryBarColor)
		surface.DrawRect( 0, self:GetTall() - 30, self.SecondaryBarWidth, 30)
	end
	//Primary hover
	if self.Hovering and not self.ActiveBet then
		local x,y = self.OddsLbl:GetPos()
		surface.SetDrawColor(self.PrimaryBarColor)
		surface.DrawRect( x - 30, y + 20, self.OddsLbl:GetWide() + 60, self.OddsLbl:GetTall() - 40)
		
		surface.SetFont("Bebas40Font")
		local w,h = surface.GetTextSize("Place Bet")
		local offset = (self:GetWide() / 2)
		draw.DrawText( "Place Bet", "Bebas40Font", offset, y + 40, self.TextColor, TEXT_ALIGN_CENTER ) 
	end
	//Results bar
	if self.BetWon or self.BetLost then
		local x,y = self.OddsLbl:GetPos()
		surface.SetDrawColor(self.BetWon and self.WinningColor or self.LosingColor)
		surface.DrawRect( x - 5, y + 2, self.OddsLbl:GetWide() + 10, self.OddsLbl:GetTall() - 4)
	end
	
	//Flash bar
	if self.ActiveBet and self.FlashColor then
		self.CurrentFlashAlpha = math.Approach(self.CurrentFlashAlpha or 0, 0, FrameTime() * 400)
		surface.SetDrawColor(self.FlashColor.r,self.FlashColor.g,self.FlashColor.b,self.CurrentFlashAlpha )
		surface.DrawRect( 0, 0, self:GetWide(), self:GetTall() - 35)
	end
	
	//Timer bar
	if self.Timer then
		local timeremaining = math.Clamp(self.EndTime - CurTime(),0,self.Timer)
		local barwidth = (self:GetWide() / self.Timer) * timeremaining
		surface.SetDrawColor(self.PrimaryBarColor)
		surface.DrawRect( 0, self:GetTall() - 38,math.Clamp(barwidth,0,self:GetWide()), 3)
		if barwidth <= 0 then self.Timer = nil self.EndTime = nil end
	end
end

function BetPanel:PaintOver()
end

function BetPanel:OnCursorEntered() 
	self:AlphaTo(255,0.5,0) 
	if not self.ActiveBet then self.OddsLbl:SetVisible(false) end
	self.Hovering = true 
end

function BetPanel:OnCursorExited() 
	self:AlphaTo(200,0.5,0) 
	if not self.ActiveBet then self.OddsLbl:SetVisible(true) end
	self.Hovering = false 
end

function BetPanel:ToggleSelect(select)
	if select then
		self.SelectFadeAlpha = 255
		self.CurrentSelection = true
	else
		self.CurrentSelection = false
	end
end

function BetPanel:ColorWithCurrentAlpha(c)
	local r,g,b = c.r,c.g,c.b
	return Color(r,g,b,self.CurrentFlashAlpha)
end
derma.DefineControl("BetPanel", "Betting info panel", BetPanel, "DImageButton")

/*---------------------------------------------------------
End of Bet Panel
---------------------------------------------------------*/