/*---------------------------------------------------------
  START PSBettingButton
---------------------------------------------------------*/
local PSBettingButton = {}

function PSBettingButton:Init()
	self:SetDrawBackground(false)
	self:SetDrawBorder(false)
	self:SetSize(200, 70)
	
	self.TextFont = "Bebas30Font"
	self.BackColor = Color(47,52,70)
	self.TextColor = Color(217,217,217)
	self.HoverColor = Color(57,67,86)
	self.HoverTextColor = color_white
	self.Hovering = false
	self.TextWidth = 50
	
	self.HeaderLbl = vgui.Create("DLabel", self)
	self.HeaderLbl:SetFont(self.TextFont)
	self.HeaderLbl:SetColor(self.TextColor)
end

function PSBettingButton:SetColor(color)
	if not type(color) == "color" then return end
	self.NoAction = true
	self.HoverColor = color
end

function PSBettingButton:SetText(text)
	self.HeaderLbl:SetText(text)
	self.HeaderLbl:SizeToContents()
	
	surface.SetFont(self.TextFont)
	self.TextWidth = surface.GetTextSize(text)
	
	local wide = math.Clamp(self.TextWidth, 200, ScrW() / 2)
	self:SetWide(wide)
end

function PSBettingButton:PerformLayout()
	local remainder = self:GetWide() - self.TextWidth
	self.HeaderLbl:SetPos(remainder / 2, 20 )
end

function PSBettingButton:Paint()
	surface.SetDrawColor(self.BackColor)
	
	if self.Hovering or self.CurrentSelection then
		if !self:FadeInComplete() then
		self.ApproachR = math.Approach( self.ApproachR, self.HoverColor.r, FrameTime() * 200 )
		self.ApproachG = math.Approach( self.ApproachG, self.HoverColor.g, FrameTime() * 200 )
		self.ApproachB = math.Approach( self.ApproachB, self.HoverColor.b, FrameTime() * 200 )	
		BETTING.CurrentTabLineColor = Color(self.ApproachR,self.ApproachG,self.ApproachB)
		end
		surface.SetDrawColor(self.ApproachR,self.ApproachG,self.ApproachB)
	end
	surface.DrawRect( 0, 0, self:GetWide(), self:GetTall())
end

function PSBettingButton:OnCursorEntered()
	self.ApproachR = self.BackColor.r
	self.ApproachG = self.BackColor.g
	self.ApproachB = self.BackColor.b
	self.Hovering = true
	if not self.NoAction and not self.CurrentSelection then
	self.HeaderLbl:SetColor(self.HoverTextColor)
	end
end

function PSBettingButton:OnCursorExited()
	self.Hovering = false
	if not self.NoAction and not self.CurrentSelection then
	self.HeaderLbl:SetColor(self.TextColor)
	end
end

function PSBettingButton:ToggleSelect(select)
	if select then 
	if !self:FadeInComplete() and not self.Hovering then
	self.ApproachR = self.BackColor.r
	self.ApproachG = self.BackColor.g
	self.ApproachB = self.BackColor.b
	end
	self.CurrentSelection = true
	else self.CurrentSelection = false end
end

function PSBettingButton:FadeInComplete()
local complete = true
if (self.ApproachR != self.HoverColor.r) then complete = false end
if (self.ApproachG != self.HoverColor.g) then complete = false end
if (self.ApproachB != self.HoverColor.b) then complete = false end
return complete
end

derma.DefineControl("PSBettingButton", "Pointshop Betting Button", PSBettingButton, "DImageButton")

/*---------------------------------------------------------
  End PSBettingButton
---------------------------------------------------------*/