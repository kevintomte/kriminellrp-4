BETTING.Theme = {}
--
-- Pointshop Betting Theme
--

BETTING.Theme.ControlColor = Color(38, 41, 49, 255) --Main control background color
BETTING.Theme.NoticePrefixColor = Color(117,0,235) --Notice prefix color for chat messages
BETTING.Theme.NoticeTextColor = Color(77,121,255) --Notice text color for chat messages

BETTING.Settings = {}
--
-- Pointshop Betting Core Settings
--
BETTING.Settings.BetMultiplier = 2 --Multiplier for pointshop bets (e.g. 100x2 = 200 if bet wins)
BETTING.Settings.MinimumBet = 10 --Minimum points allowed to bet (must be at least 1)
--Set the MaximumBet setting to 0 disable the limit altogether
BETTING.Settings.MaximumBet = 500 --Maximum points allowed to bet
BETTING.Settings.AllowAllInBets = false --Allow players to bet all if they want to bet more than the maximum
BETTING.Settings.MinimumPlayersForBetting = 5 --Players needed on the server to allow bets
BETTING.Settings.MinimumAlivePlayersForBetting = 3 --Players needed alive to allow bets
BETTING.Settings.OnlyAllowBettingAtRoundStartTime = 30 --Only allow bets for first x seconds of round
--Set OnlyAllowBetsSelf and AllowBetsOnOthers to false to enable the team betting mode.
BETTING.Settings.OnlyAllowBetsSelf = true --Players bet on themselves and must win and stay alive
BETTING.Settings.AllowBetsOnOthers = true --Players can bet on themselves or other players in this mode
BETTING.Settings.HideBetScreenWhilstAlive = true  --Hide bet panel after round start time is over?
BETTING.Settings.HideBetScreenWhilstDead = false  --Hide bet panel when dead/spectating and have active bet
BETTING.Settings.ShowBetResultsTime = 12 --How long to show the bet results (in seconds)
BETTING.Settings.FKeyShowCursor = "F2" --Set to F1,F1,F3,F4 or "" to disable
BETTING.Settings.ShowFKeyShowCursorMessage = true --Show message on bet panel notifying players of the FKey
BETTING.Settings.DisableCursorAfterBetPlaced = true --Hide cursor after bet is placed
BETTING.Settings.ShowHighestWinnerOrLoser = true --Show notice about highest winner/loser of the round
BETTING.Settings.ShowHighestWinnerOrLoserTime = 12 --How long to show the highest winner/loser (in seconds)
BETTING.Settings.ShowOnRight = false --Set to false for left or true for right of screen
BETTING.Settings.HideWhenChatBoxOpen = true --Hide the bet UI when the chatbox is open

BETTING.Settings.NoticePrefix = "[BETTING]" --Prefix shown before chat notices
BETTING.Settings.ShowNotificationsAboutOtherBets = true --Show notices about other players betting?
BETTING.Settings.ShowNotificationsAboutNoBets = true --Show a notice when nobody places a bet?


BETTING.Settings.EnableSounds = true --Play the menu sounds?
BETTING.Settings.BetPlacedSound = "http://fastdl.friendlyplayers.com/as/purchase.wav" --Player placed bet
--Bet result music, you can specify multiple files, seperate each with a comma.
--Sounds can be local or from the web (.mp3 or .wav files)
BETTING.Settings.WinningBetMusic = {"http://fastdl.friendlyplayers.com/as/win1.mp3",
"http://fastdl.friendlyplayers.com/as/win2.mp3"}
BETTING.Settings.LosingBetMusic = {"http://fastdl.friendlyplayers.com/as/lose1.mp3",
"http://fastdl.friendlyplayers.com/as/lose2.mp3"}


BETTING.Settings.LogBettingToULXFile = true --Log bet wins/loses to ULX log file in data/ulx_logs/...

//BETTING.Settings.CustomCanBetFunction = function(ply,plybeton)
//	if not ply:IsUserGroup("vipmembers") then return false,"Only VIP's can use the betting system!"
//	else return true end
//end