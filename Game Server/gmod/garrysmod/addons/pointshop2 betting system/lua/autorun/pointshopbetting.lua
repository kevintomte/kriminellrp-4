if SERVER then
	AddCSLuaFile()
	AddCSLuaFile('cl_pointshopbetting.lua')
	AddCSLuaFile('sh_bettingconfig.lua')
	AddCSLuaFile('sh_pointshopbetting.lua')
	AddCSLuaFile('cl_bettingfonts.lua')
	
	--Add panel files
	AddCSLuaFile('panels/cl_betpanel.lua')
	AddCSLuaFile('panels/cl_betbutton.lua')
	AddCSLuaFile('panels/cl_winnerpanel.lua')
	
	--Send gamemode scripts to client for shared use
	for k,v in pairs(file.Find("betgamemodes/*.lua","LUA")) do
		AddCSLuaFile("betgamemodes/" .. v)
	end
	
	--Add server files
	include('sv_pointshopbetting.lua')
	
	--Add resources
	resource.AddFile("resource/fonts/BebasNeue.ttf")
	resource.AddFile("resource/fonts/OpenSansC.ttf")
end

if CLIENT then
	include('cl_pointshopbetting.lua')
end