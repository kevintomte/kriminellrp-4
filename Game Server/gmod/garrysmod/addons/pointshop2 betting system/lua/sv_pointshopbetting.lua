--Pointshop Betting Main Server Dist
BETTING = {}
BETTING.CurrentBets = {}
include('sh_bettingconfig.lua')
include('sh_pointshopbetting.lua')

util.AddNetworkString("PSBetting_UpdateTotal")

function BETTING.InitGamemodeScript()
	if file.Exists(string.format("betgamemodes/%s.lua",gmod.GetGamemode().FolderName),"LUA") then
		include(string.format("betgamemodes/%s.lua",gmod.GetGamemode().FolderName))
	elseif gmod.GetGamemode().BaseClass and file.Exists(string.format("betgamemodes/%s.lua",gmod.GetGamemode().BaseClass.FolderName),"LUA") then
		include(string.format("betgamemodes/%s.lua",gmod.GetGamemode().BaseClass.FolderName))
	else ServerLog("[POINTSHOP BETTING] Warning: No gamemode script found. Developers must call pointshop betting functions manually.") end
end
hook.Add("InitPostEntity","BETTING_InitGamemodeScript",BETTING.InitGamemodeScript)

function BETTING.NewBet(ply, cmd, args)
	if not IsValid(ply) or not args or not (PS or Pointshop2) then return end
	if not BETTING.IsTeamValid(BETTING.FirstTeam) or not BETTING.IsTeamValid(BETTING.SecondTeam) then
		ErrorNoHalt("[POINTSHOP BETTING] Error: Betting teams are not configured.")
		return
	end

	local beton = args[1]
	local amount = args[2]
	local allinbet = tobool(tonumber(args[3]))
	local plybeton

	if BETTING.Settings.AllowBetsOnOthers then 
		plybeton = player.GetByID(args[4]) 
		if not IsValid(plybeton) then return end 
		if not plybeton:Alive() or plybeton:Team() == TEAM_SPECTATOR then BETTING.ShowChatNotice(ply,string.format("%s has already died this round!",plybeton:Nick())) return end
		beton = BETTING.GetPlayerTeam(plybeton)
	end
	
	if not tonumber(beton) or not tonumber(amount) then return end
	beton = math.floor(beton) amount = math.floor(amount)
	//All in bet
	if allinbet then amount = BETTING.GetPoints(ply) end
	
	if not (beton == BETTING.GetTeamID(BETTING.FirstTeam)) and not (beton == BETTING.GetTeamID(BETTING.SecondTeam)) then return end
	if BETTING.Settings.OnlyAllowBetsSelf and not BETTING.Settings.AllowBetsOnOthers and (beton != BETTING.GetPlayerTeam(ply)) then return end
	
	if BETTING.HasPlayerBet(ply) then BETTING.ShowChatNotice(ply,"You have already placed a bet for this round!") return end
	if BETTING.Settings.MinimumPlayersForBetting and (BETTING.Settings.MinimumPlayersForBetting > 0) then
		if (#player.GetAll() < BETTING.Settings.MinimumPlayersForBetting) then BETTING.ShowChatNotice(ply,string.format("There must be at least %i players online to enable betting.",BETTING.Settings.MinimumPlayersForBetting)) return end
	end
	if BETTING.Settings.MinimumAlivePlayersForBetting and (BETTING.Settings.MinimumAlivePlayersForBetting > 0) then
		if (#BETTING.FindAlivePlayers() < BETTING.Settings.MinimumAlivePlayersForBetting) then BETTING.ShowChatNotice(ply,string.format("There must be at least %i alive players enable betting.",BETTING.Settings.MinimumAlivePlayersForBetting)) return end
	end

	if (amount < BETTING.Settings.MinimumBet) or (amount < 1) then BETTING.ShowChatNotice(ply,string.format("The minimum bet is %s %s",BETTING.FormatNumber(BETTING.Settings.MinimumBet),BETTING.GetPointsName())) return end
	local IsMaxBet = BETTING.Settings.MaximumBet and BETTING.Settings.MaximumBet > 0
	
	if not (BETTING.Settings.AllowAllInBets and allinbet) then
		if IsMaxBet and (amount > BETTING.Settings.MaximumBet) then BETTING.ShowChatNotice(ply,string.format("The maximum bet is %s %s",BETTING.FormatNumber(BETTING.Settings.MaximumBet),BETTING.GetPointsName())) return end
		if not BETTING.HasPoints(ply, amount) then BETTING.ShowChatNotice(ply,string.format("You cant afford to bet %s %s",BETTING.FormatNumber(amount),BETTING.GetPointsName())) return end
	end
	
	local canbet,msg = hook.Call("PlayerCanBet",nil,ply)
	if not canbet then if msg then BETTING.ShowChatNotice(ply,msg) end return end
	
	if BETTING.Settings.CustomCanBetFunction then
		local customcanbet,msg = BETTING.Settings.CustomCanBetFunction(ply, plybeton)
		if not customcanbet then if msg then BETTING.ShowChatNotice(ply,msg) end return end
	end
	
	table.insert(BETTING.CurrentBets, {ply = ply, team = beton, amount = amount, plybeton = plybeton, allinbet = allinbet})
	 BETTING.TakePoints(ply, amount)
	//BETTING.ShowChatNotice(ply,string.format("Your bet on %s for %s %s has been placed successfully.",team.GetName(beton),BETTING.FormatNumber(amount),BETTING.GetPointsName()))
	
	SendUserMessage("PSBetting_BetPlaced", ply, beton, amount, plybeton)
	BETTING.SendPlayerBetTotals()
	if BETTING.Settings.ShowNotificationsAboutOtherBets then
		for k,v in pairs(player.GetAll()) do
			BETTING.ShowChatNotice(v, string.format("%s has placed a bet on %s for %s %s %s",
			ply:Nick(),plybeton == ply and "themselves" or plybeton and plybeton:Nick() or BETTING.Settings.OnlyAllowBetsSelf and "themselves" or BETTING.GetTeamName(beton),BETTING.FormatNumber(amount),BETTING.GetPointsName(),
			(BETTING.Settings.AllowAllInBets and allinbet) and "(All-In!)" or ""))
		end
	end
end
concommand.Add("PSBetting_NewBet",BETTING.NewBet)

function BETTING.FinishBets(winner, cancel)
	if not cancel and not (winner == BETTING.GetTeamID(BETTING.FirstTeam)) and not (winner == BETTING.GetTeamID(BETTING.SecondTeam)) then
		ErrorNoHalt("[POINTSHOP BETTING] Error: FinishBets called with invalid team as winner.")
		return
	end
	
	--Find winners and losers
	local highest,highestply,winorlose
	for k,v in pairs(BETTING.CurrentBets) do
		if not IsValid(v.ply) then continue end
		local forcelose = false
		if BETTING.Settings.OnlyAllowBetsSelf and not BETTING.Settings.AllowBetsOnOthers and not v.ply:Alive() then forcelose = true
		elseif BETTING.Settings.AllowBetsOnOthers then if not IsValid(v.plybeton) or not v.plybeton:Alive() then forcelose = true end end
		
		//Update correct teams
		local betteam = v.team
		if BETTING.Settings.OnlyAllowBetsSelf then betteam = BETTING.GetPlayerTeam(v.ply) end
		if BETTING.Settings.AllowBetsOnOthers and IsValid(v.plybeton) then betteam = BETTING.GetPlayerTeam(v.plybeton) end			
		
		if (betteam == winner) and not forcelose and not cancel then
			local winnings = v.amount * (BETTING.Settings.BetMultiplier or 2)
			BETTING.GivePoints(v.ply, math.floor(winnings))
			SendUserMessage("PSBetting_BetWon",v.ply, betteam, winnings)
			if not highest then highest = winnings highestply = v.ply winorlose = true
			elseif (winnings > highest) then highest = winnings highestply = v.ply winorlose = true end
			BETTING.LogBetResult(v.ply, betteam, v.plybeton, winnings, true, v.allinbet)
		else
			SendUserMessage("PSBetting_BetLost",v.ply, betteam, v.amount)
			if not highest then highest = v.amount highestply = v.ply winorlose = false
			elseif (v.amount > highest) then highest = v.amount highestply = v.ply winorlose = false end
			BETTING.LogBetResult(v.ply, betteam, v.plybeton, v.amount, false, v.allinbet)
		end
	end
	if #BETTING.CurrentBets < 1 and BETTING.Settings.ShowNotificationsAboutNoBets then
		for k,v in pairs(player.GetAll()) do
			BETTING.ShowChatNotice(v,"Nobody placed any bets that round!")
		end
	elseif highest and highestply and BETTING.Settings.ShowHighestWinnerOrLoser then
		SendUserMessage("PSBetting_Highest",player.GetAll(), highestply, winorlose, highest)
	end
	SendUserMessage("PSBetting_ResetBetTotal",player.GetAll())
	
	BETTING.CurrentBets = {}
end

function BETTING.SendPlayerBetTotals(ply)
	if #BETTING.CurrentBets < 1 then return end
	net.Start("PSBetting_UpdateTotal")
		net.WriteInt(BETTING.CalculateTotalBets(), 32)
	if IsValid(ply) then net.Send(ply)
	else net.Broadcast() end
end
hook.Add("PlayerInitialSpawn","BETTING_SendPlayerBetTotals",BETTING.SendPlayerBetTotals)

function BETTING.HasPlayerBet(ply)
	for k,v in pairs(BETTING.CurrentBets) do
		if (v.ply == ply) then return true end
	end
	return false
end

function BETTING.CalculateTotalBets()
	local total = 0
	for k,v in pairs(BETTING.CurrentBets) do
		total = (total + v.amount)
	end
	return total
end

function BETTING.FindAlivePlayers()
	local players = {}
	for k,v in pairs(player.GetAll()) do
		if v:Alive() then table.insert(players, v) end
	end
	return players
end

function BETTING.ShowChatNotice(ply, text)
	if not IsValid(ply) or not text then return end
	SendUserMessage("BETTING_PointshopBettingNotice",ply,text)
end

function BETTING.LogBetResult(ply, beton, plybeton, amount, win, allinbet)
	if not BETTING.Settings.LogBettingToULXFile or not ulx or not ulx.logString then return end
	ulx.logString(string.format("%s (%s) bet on %s and %s %s %s %s",ply:Nick(),ply:SteamID(),
	plybeton == ply and "themselves" or plybeton and (IsValid(plybeton) and plybeton:Nick() or "another player") or BETTING.Settings.OnlyAllowBetsSelf and "themselves" or BETTING.GetTeamName(beton),win and "won" or "lost",BETTING.FormatNumber(amount),BETTING.GetPointsName(),
	allinbet and "(All-in bet)" or ""),true)
end