if gmod.GetGamemode().Author == "BlackVoid" then include('betgamemodes/blackvoidsdeathrun.lua') return
elseif gmod.GetGamemode().Author == "Arizard" then include('betgamemodes/arizardsdeathrun.lua') return end
--Pointshop betting gamemode scripts are SHARED. Hooks must return the same data on both server and client.
--This script is for Mr Gash's deathrun. For BlackVoid's deathrun see blackvoiddeathrun.lua or arizardsdeathrun.lua for Arizard's gamemode

--Gamemode scripts MUST ALWAYS specify the first and second teams to bet on with these settings:
BETTING.FirstTeam = TEAM_DEATH
BETTING.SecondTeam = TEAM_RUNNER
//You could use this file to load custom settings based on the gamemode e.g.
//BETTING.Settings.MinimumPlayersForBetting = 5

local function PlayerCanBetDeathrun(ply)
	--BETTING.Settings.OnlyAllowBettingAtRoundStartTime enabled
	if tonumber(BETTING.Settings.OnlyAllowBettingAtRoundStartTime) and BETTING.Settings.OnlyAllowBettingAtRoundStartTime > 0 then
		if GetGlobalInt( "Deathrun_RoundPhase") == 1 then return true end
		if GetGlobalInt( "Deathrun_RoundPhase") != 2 then return false,"You cant bet whilst a new round is preparing." end
		local roundtime = tonumber(GetConVarNumber( "dr_roundtime_seconds" ))
		if roundtime <= 0 then roundtime = 360 end
		if (roundtime - BETTING.Settings.OnlyAllowBettingAtRoundStartTime) > gmod.GetGamemode().GetRoundTime() then
			return false,string.format("You must place a bet in the first %i seconds of the round!",BETTING.Settings.OnlyAllowBettingAtRoundStartTime)
		end
		return true
	end
	
	--BETTING.Settings.OnlyAllowBettingAtRoundStartTime disabled
	if ply:Alive() then return false,"Players must be dead or spectating to bet." end
	//2 is round active, why aren't the round enums shared?
	if GetGlobalInt( "Deathrun_RoundPhase") != 2 then return false,"You cant bet whilst a new round is preparing." end
	return true
end
hook.Add("PlayerCanBet","PlayerCanBetDeathrun",PlayerCanBetDeathrun)

--SERVER hooks
--Every outcome must call BETTING.FinishBets else the bet window will not be closed
local function DeathrunRoundEndBets(round,winner)
	if not round or not (round == ROUND_ENDING) or not winner then return end
	if (winner == 123) then BETTING.FinishBets(winner, true)
	else BETTING.FinishBets(winner) end
end
hook.Add("OnRoundSet","DeathrunRoundEndBets",DeathrunRoundEndBets)