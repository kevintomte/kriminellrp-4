--Pointshop betting gamemode scripts are SHARED. Hooks must return the same data on both server and client.
--This script is for Arizards's deathrun gamemode.

--Gamemode scripts MUST ALWAYS specify the first and second teams to bet on with these settings:
BETTING.FirstTeam = TEAM_DEATH
BETTING.SecondTeam = TEAM_RUNNER
//You could use this file to load custom settings based on the gamemode e.g.
//BETTING.Settings.MinimumPlayersForBetting = 5
local function PlayerCanBetAZDeathrun(ply)
	--BETTING.Settings.OnlyAllowBettingAtRoundStartTime enabled
	if tonumber(BETTING.Settings.OnlyAllowBettingAtRoundStartTime) and BETTING.Settings.OnlyAllowBettingAtRoundStartTime > 0 then
		if BETTING.EndBetsTime and CurTime() <= BETTING.EndBetsTime then return true end
		return false,string.format("You must place a bet in the first %i seconds of the round!",BETTING.Settings.OnlyAllowBettingAtRoundStartTime)
	end
	
	--BETTING.Settings.OnlyAllowBettingAtRoundStartTime disabled
	if ply:Alive() then return false,"Players must be dead or spectating to bet." end
	if ROUND:GetCurrent() != ROUND_ACTIVE then return false,"You cant bet whilst a new round is preparing." end
	return true
end
hook.Add("PlayerCanBet","PlayerCanBetAZDeathrun",PlayerCanBetAZDeathrun)

local function AllowBetsWhenRoundBegins()
	BETTING.EndBetsTime = CurTime() + BETTING.Settings.OnlyAllowBettingAtRoundStartTime or 30
end
hook.Add("DeathrunBeginPrep","AllowBetsWhenRoundBegins",AllowBetsWhenRoundBegins)

--SERVER hooks
--Every outcome must call BETTING.FinishBets else the bet window will not be closed
local function DeathrunRoundEndBets(winner)
	if not SERVER then return end
	if (winner != BETTING.FirstTeam and winner != BETTING.SecondTeam) then
		BETTING.FinishBets(winner, true)
	else
		BETTING.FinishBets(winner)
	end
end
hook.Add("DeathrunRoundWin","DeathrunRoundEndBets",DeathrunRoundEndBets)