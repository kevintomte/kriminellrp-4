--Pointshop betting gamemode scripts are SHARED. Hooks must return the same data on both server and client.
--This script is for BlackVoid's deathrun gamemode.

--Gamemode scripts MUST ALWAYS specify the first and second teams to bet on with these settings:
BETTING.FirstTeam = TEAM_DEATH
BETTING.SecondTeam = TEAM_RUN
//You could use this file to load custom settings based on the gamemode e.g.
//BETTING.Settings.MinimumPlayersForBetting = 5
local function PlayerCanBetBVDeathrun(ply)
	--BETTING.Settings.OnlyAllowBettingAtRoundStartTime enabled
	if tonumber(BETTING.Settings.OnlyAllowBettingAtRoundStartTime) and BETTING.Settings.OnlyAllowBettingAtRoundStartTime > 0 then
		if BETTING.EndBetsTime and CurTime() <= BETTING.EndBetsTime then return true end
		return false,string.format("You must place a bet in the first %i seconds of the round!",BETTING.Settings.OnlyAllowBettingAtRoundStartTime)
	end
	
	--BETTING.Settings.OnlyAllowBettingAtRoundStartTime disabled
	if ply:Alive() then return false,"Players must be dead or spectating to bet." end
	if gmod.GetGamemode():GetGameState() != STATE_ROUND_IN_PROGRESS then return false,"You cant bet whilst a new round is preparing." end
	return true
end
hook.Add("PlayerCanBet","PlayerCanBetBVDeathrun",PlayerCanBetBVDeathrun)

local function AllowBetsWhenRoundBegins()
	BETTING.EndBetsTime = CurTime() + BETTING.Settings.OnlyAllowBettingAtRoundStartTime or 30
end
hook.Add("OnRoundStart","AllowBetsWhenRoundBegins",AllowBetsWhenRoundBegins)

--SERVER hooks
--Every outcome must call BETTING.FinishBets else the bet window will not be closed
if SERVER then
	local OldOnRoundEnd = gmod.GetGamemode().OnRoundEnd
	local function BVDeathrunEndRoundBets(gm,winner)
		OldOnRoundEnd(gm,winner)
		if not winner then return end
		if winner != TEAM_DEATH and winner != TEAM_RUN then BETTING.FinishBets(winner, true)
		else BETTING.FinishBets(winner) end
	end
	gmod.GetGamemode().OnRoundEnd = BVDeathrunEndRoundBets
end
//OnRoundEnd hook is broken in BlackVoids gamemode. (Serverside) It doesn't send the winner argument
//so we must override the gamemode function.