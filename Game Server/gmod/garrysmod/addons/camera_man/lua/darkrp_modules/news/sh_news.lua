NewsTickers = NewsTickers or {}
local jobInfo = {
	color = Color(123, 191, 106, 255),
	model = {
		"models/player/suits/male_07_open.mdl",
		"models/player/suits/male_01_open.mdl",
		"models/player/suits/male_02_open.mdl",
		"models/player/suits/male_03_open.mdl",
		"models/player/suits/male_04_open.mdl",
		"models/player/suits/male_05_open.mdl",
		"models/player/suits/male_06_open.mdl",
		"models/player/suits/male_08_open.mdl",
		"models/player/suits/male_09_open.mdl"
	},
	description = "A guy with a camera that spreads news.",
	weapons = {"news_camera"},
	max = NewsConfig.jobMax,
	salary = NewsConfig.jobPay,
	admin = 0,
	vote = NewsConfig.jobIsVoted,
	command = "reporter",
	category = "Civila Medborgare",
	sortOrder = 3
}
if (NewsConfig.jobIsVIP) then
	jobInfo.customCheck = function(client)
		if (client:IsAdmin()) then
			return true
		end
		for k, v in pairs(NewsConfig.vipGroups) do
			if ((client.CheckGroup and client:CheckGroup(v)) or client:IsUserGroup(v) or client:GetNWString("EV_UserGroup") == v or (client.GetUserGroup and client:GetUserGroup() == v)) then
				return true
			end
		end
		return false
	end
end
TEAM_REPORTER = AddExtraTeam("Reporter", jobInfo)
local entityInfo = {
	ent = "news_tv",
	model = "models/props_phx/rt_screen.mdl",
	price = NewsConfig.entityPrice or 500,
	max = NewsConfig.entityMax or 2,
	cmd = "nyhets tv",
	category = "Jobb Specialare",
	customCheck = function(client)
		if (GetGlobalBool("tvDisabled") and !client:IsAdmin()) then
			return false
		end
		return true
	end
}
if (NewsConfig.entityIsVIP) then
	entityInfo.customCheck = function(client)
		if (GetGlobalBool("tvDisabled")) then
			return false
		end
		if (client:IsAdmin()) then
			return true
		end
		for k, v in pairs(NewsConfig.vipGroups) do
			if ((client.CheckGroup and client:CheckGroup(v)) or client:IsUserGroup(v) or client:GetNWString("EV_UserGroup") == v or (client.GetUserGroup and client:GetUserGroup() == v)) then
				return true
			end
		end
		return false
	end
end
DarkRP.createEntity("Nyhets TV", entityInfo)