﻿-----------------------------------------------------------------
-- @package		Dozi
-- @author		Richard
-- @build 		v1.3
-- @release		09.19.2015
-----------------------------------------------------------------

Dozi = Dozi or {}

-----------------------------------------------------------------
-- Resources
-----------------------------------------------------------------

resource.AddFile( "materials/dozi/dozi_glass_1.png" )
resource.AddFile( "materials/dozi/dozi_glass_2.png" )
resource.AddFile( "materials/dozi/dozi_blood_1.png" )
resource.AddFile( "materials/dozi/dozi_blood_2.png" )
resource.AddFile( "materials/dozi/dozi_blood_3.png" )
resource.AddFile( "materials/dozi/dozi_item_hand.png")
resource.AddFile( "materials/dozi/dozi_gui_deaths.png" )
resource.AddFile( "materials/dozi/dozi_gui_kills.png" )
resource.AddFile( "materials/dozi/dozi_gui_killed.png" )
resource.AddFile( "resource/fonts/pricedown.ttf" )
resource.AddFile( "resource/fonts/amburegul.ttf" )
resource.AddFile( "resource/fonts/dashley.ttf" )
resource.AddFile( "sound/dozi/dozi_dozi_death.mp3" )
resource.AddFile( "sound/dozi/dozi_gta_death.mp3" )
resource.AddFile( "sound/dozi/dozi_re_death.mp3" )

-----------------------------------------------------------------
-- Network Strings
-----------------------------------------------------------------

util.AddNetworkString( "DoziDetectDeath" )
util.AddNetworkString( "DoziFetchDeathInfo" )
util.AddNetworkString( "DoziMessageSet" )

local Player = FindMetaTable( "Player" )

-----------------------------------------------------------------
-- Message System
-----------------------------------------------------------------
	
function Dozi:Broadcast(...)
	local args = {...}
	net.Start( "DoziMessageSet" )
	net.WriteTable( args )
	net.Broadcast()
end

function Player:PlayerMsg(...)
	local args = {...}
	net.Start( "DoziMessageSet" )
	net.WriteTable( args )
	net.Send( self )
end