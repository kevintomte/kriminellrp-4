--[[---------------------------------------------------------
	Name: Settings
-----------------------------------------------------------]]
local GroupDonator		= { "superadmin", "admin", "moderator", "doantor" }


--[[---------------------------------------------------------
	Name: Printers
-----------------------------------------------------------]]
DarkRP.createEntity( "Amber Printer", {
	ent = "fg_amber_printer",
	model = "models/props_c17/consolebox01a.mdl",
	price = 5000,
	max = 4,
	cmd = "buyamberprinter",
	category = "Pengar skrivare"
})

DarkRP.createEntity( "Sapphire Printer", {
	ent = "fg_sapphire_printer",
	model = "models/props_c17/consolebox01a.mdl",
	price = 990,
	max = 4,
	cmd = "buysapphireprinter",
	category = "Pengar skrivare"
})

DarkRP.createEntity( "Ruby Printer", {
	ent = "fg_ruby_printer",
	model = "models/props_c17/consolebox01a.mdl",
	price = 25000,
	max = 4,
	cmd = "buyrubyprinter",
	category = "Pengar skrivare"
})

DarkRP.createEntity( "Emerald Printer", {
	ent = "fg_emerald_printer",
	model = "models/props_c17/consolebox01a.mdl",
	price = 50000,
	max = 4,
	cmd = "buyemeraldprinter",
	customCheck = function(ply) return
		table.HasValue( GroupDonator, ply:GetNWString("usergroup") )
	end,
	customCheckFailMsg = "This entity is restricted to donator and higher!",	
	category = "Pengar skrivare"
})

DarkRP.createEntity("Vapen labb", {
    ent = "gunlab",
    model = "models/props_c17/TrapPropeller_Engine.mdl",
    price = 1499,
    max = 1,
    cmd = "vapenlabb",
    allowed = {TEAM_ORTEN, TEAM_ORTENSLEDARE, TEAM_AFA, TEAM_AFASLEDARE, TEAM_SMR, TEAM_SMRLEDARE, TEAM_MAFFIA, TEAM_MAFFIALEDARE},
	category = "Drog labb"
})

DarkRP.createEntity("Mini Drog Labb", {
    ent = "drug_lab",
    model = "models/props_lab/crematorcase.mdl",
    price = 399,
    max = 3,
    cmd = "minidroglabb",
    allowed = TEAM_AKOKARE,
	category = "Drog labb"
})

DarkRP.createEntity("Gas Canister", {
	ent = "eml_gas",
	model = "models/props_c17/canister01a.mdl",
	price = 199,
	max = 20,
	cmd = "gascanister",
    allowed = TEAM_AKOKARE,
	category = "Drog labb"
})

DarkRP.createEntity("Liquid Iodine", {
	ent = "eml_iodine",
	model = "models/props_lab/jar01b.mdl",
	price = 59,
	max = 20,
	cmd = "liquidiodine",
    allowed = TEAM_AKOKARE,
	category = "Drog labb"
})

DarkRP.createEntity("Jar", {
	ent = "eml_jar",
	model = "models/props_lab/jar01a.mdl",
	price = 0,
	max = 20,
	cmd = "jar",
    allowed = TEAM_AKOKARE,
	category = "Drog labb"
})

DarkRP.createEntity("Muriatic Acid", {
	ent = "eml_macid",
	model = "models/props_junk/garbage_plasticbottle001a.mdl",
	price = 79,
	max = 20,
	cmd = "muriaticacid",
    allowed = TEAM_AKOKARE,
	category = "Drog labb"
})

DarkRP.createEntity("Pot", {
	ent = "eml_pot",
	model = "models/props_c17/metalPot001a.mdl",
	price = 29,
	max = 20,
	cmd = "pot",
    allowed = TEAM_AKOKARE,
	category = "Drog labb"
})

DarkRP.createEntity("Special Pot", {
	ent = "eml_spot",
	model = "models/props_c17/metalPot001a.mdl",
	price = 34,
	max = 20,
	cmd = "specialpot",
    allowed = TEAM_AKOKARE,
	category = "Drog labb"
})

DarkRP.createEntity("Stove", {
	ent = "eml_stove",
	model = "models/props_c17/furnitureStove001a.mdl",
	price = 299,
	max = 5,
	cmd = "stove",
    allowed = TEAM_AKOKARE,
	category = "Drog labb"
})

DarkRP.createEntity("Liquid Sulfur", {
	ent = "eml_sulfur",
	model = "models/props_lab/jar01b.mdl",
	price = 49,
	max = 20,
	cmd = "liquidsulfur",
    allowed = TEAM_AKOKARE,
	category = "Drog labb"
})

DarkRP.createEntity("Water", {
	ent = "eml_water",
	model = "models/props_junk/garbage_plasticbottle003a.mdl",
	price = 19,
	max = 20,
	cmd = "water",
    allowed = TEAM_AKOKARE,
	category = "Drog labb"
})

DarkRP.createEntity("Heavy Armor", {
	ent = "heavy kevlar armor",
    model = "models/combine_vests/bogvest.mdl",
	price = 699,
	max = 3,
	cmd = "heavyarmor",
	category = "Första Hjälpen"
})

DarkRP.createEntity("Medium Armor", {
	ent = "medium kevlar armor",
    model = "models/combine_vests/bluevest.mdl",
	price = 449,
	max = 3,
	cmd = "mediumarmor",
	category = "Första Hjälpen"
})

DarkRP.createEntity("Light Armor", {
	ent = "light kevlar armor",
    model = "models/combine_vests/obseletevest.mdl",
	price = 249,
	max = 3,
	cmd = "lightarmor",
	category = "Första Hjälpen"
})

DarkRP.createEntity("Bandages", {
	ent = "fas2_ammo_bandages",
    model = "models/weapons/w_ammobox_thrown.mdl",
	price = 19,
	max = 3,
	cmd = "bandages",
	category = "Första Hjälpen"
})

DarkRP.createEntity("Hemostats", {
	ent = "fas2_ammo_hemostats",
    model = "models/weapons/w_ammobox_thrown.mdl",
	price = 27,
	max = 3,
	cmd = "hemostats",
	category = "Första Hjälpen"
})

DarkRP.createEntity("Medical supplies", {
	ent = "fas2_ammo_medical",
    model = "models/weapons/w_ammobox_thrown.mdl",
	price = 23,
	max = 3,
	cmd = "medicalsupplies",
	category = "Första Hjälpen"
})

DarkRP.createEntity("Quikclots", {
	ent = "fas2_ammo_quikclots",
    model = "models/weapons/w_ammobox_thrown.mdl",
	price = 15,
	max = 3,
	cmd = "quikclots",
	category = "Första Hjälpen"
})


DarkRP.createEntity(".357 SIG", {
	ent = "fas2_ammo_357sig",
    model = "models/weapons/w_ammobox_thrown.mdl",
	price = 99,
	max = 3,
	cmd = "357sig",
	category = "Ammunition"
})

DarkRP.createEntity(".380 ACP", {
	ent = "fas2_ammo_380acp",
    model = "models/weapons/w_ammobox_thrown.mdl",
	price = 99,
	max = 3,
	cmd = "380acp",
	category = "Ammunition"
})

DarkRP.createEntity(".44 Magnum", {
	ent = "fas2_ammo_44mag",
    model = "models/weapons/w_ammobox_thrown.mdl",
	price = 99,
	max = 3,
	cmd = "44magnum",
	category = "Ammunition"
})

DarkRP.createEntity(".45 ACP", {
	ent = "fas2_ammo_45acp",
    model = "models/weapons/w_ammobox_thrown.mdl",
	price = 99,
	max = 3,
	cmd = "45acp",
	category = "Ammunition"
})

DarkRP.createEntity(".50 AE", {
	ent = "fas2_ammo_50ae",
    model = "models/weapons/w_ammobox_thrown.mdl",
	price = 99,
	max = 3,
	cmd = "50AE",
	category = "Ammunition"
})

DarkRP.createEntity(".50 BMG", {
	ent = "fas2_ammo_50bmg",
    model = "models/weapons/w_ammobox_thrown.mdl",
	price = 99,
	max = 3,
	cmd = "50bmg",
	category = "Ammunition"
})


DarkRP.createEntity(".500 SW", {
	ent = "fas2_ammo_454casull",
    model = "models/weapons/w_ammobox_thrown.mdl",
	price = 99,
	max = 3,
	cmd = "500sw",
	category = "Ammunition"
})

DarkRP.createEntity("10x25MM", {
	ent = "fas2_ammo_10x25",
    model = "models/weapons/w_ammobox_thrown.mdl",
	price = 99,
	max = 3,
	cmd = "10x25mm",
	category = "Ammunition"
})

DarkRP.createEntity("12 Gauge", {
	ent = "fas2_ammo_12gauge",
    model = "models/weapons/w_ammobox_thrown.mdl",
	price = 99,
	max = 3,
	cmd = "12guage",
	category = "Ammunition"
})

DarkRP.createEntity("23x75MMR", {
	ent = "fas2_ammo_23x75",
    model = "models/weapons/w_ammobox_thrown.mdl",
	price = 99,
	max = 3,
	cmd = "23x75mmr",
	category = "Ammunition"
})


DarkRP.createEntity("40MM HE", {
	ent = "fas2_ammo_40mm",
    model = "models/weapons/w_ammobox_thrown.mdl",
	price = 99,
	max = 3,
	cmd = "40mmhe",
	category = "Ammunition"
})

DarkRP.createEntity("5.45x39MM", {
	ent = "fas2_ammo_545x39",
    model = "models/weapons/w_ammobox_thrown.mdl",
	price = 99,
	max = 3,
	cmd = "45x39mm",
	category = "Ammunition"
})

DarkRP.createEntity("5.56x45MM", {
	ent = "fas2_ammo_556x45",
    model = "models/weapons/w_ammobox_thrown.mdl",
	price = 99,
	max = 3,
	cmd = "56x45mm",
	category = "Ammunition"
})

DarkRP.createEntity("7.62x39MM", {
	ent = "fas2_ammo_762x39",
    model = "models/weapons/w_ammobox_thrown.mdl",
	price = 99,
	max = 3,
	cmd = "62x39mm",
	category = "Ammunition"
})

DarkRP.createEntity("7.62x51MM", {
	ent = "fas2_ammo_762x51",
    model = "models/weapons/w_ammobox_thrown.mdl",
	price = 99,
	max = 3,
	cmd = "62x51mm",
	category = "Ammunition"
})
DarkRP.createEntity("9x18MM", {
	ent = "fas2_ammo_9x18",
    model = "models/weapons/w_ammobox_thrown.mdl",
	price = 99,
	max = 3,
	cmd = "9x18mm",
	category = "Ammunition"
})
DarkRP.createEntity("9x19M", {
	ent = "fas2_ammo_9x19",
    model = "models/weapons/w_ammobox_thrown.mdl",
	price = 99,
	max = 3,
	cmd = "9x19m",
	category = "Ammunition"
})
DarkRP.createEntity("ACOG 4x", {
	ent = "fas2_att_acog",
    model = "models/weapons/w_ammobox_thrown.mdl",
	price = 99,
	max = 3,
	cmd = "acog4x",
	category = "Vapen Tillbehör"
})

DarkRP.createEntity("CompM4", {
	ent = "fas2_att_compm4",
    model = "models/weapons/w_ammobox_thrown.mdl",
	price = 99,
	max = 3,
	cmd = "compm4",
	category = "Vapen Tillbehör"
})

DarkRP.createEntity("ELCAN C79", {
	ent = "fas2_att_c79",
    model = "models/weapons/w_ammobox_thrown.mdl",
	price = 99,
	max = 3,
	cmd = "elcanc79",
	category = "Vapen Tillbehör"
})

DarkRP.createEntity("EoTech 553", {
	ent = "fas2_att_eotech",
    model = "models/weapons/w_ammobox_thrown.mdl",
	price = 99,
	max = 3,
	cmd = "eotech553",
	category = "Vapen Tillbehör"
})

DarkRP.createEntity("Foregrip", {
	ent = "fas2_att_foregrip",
    model = "models/weapons/w_ammobox_thrown.mdl",
	price = 99,
	max = 3,
	cmd = "foregrip",
	category = "Vapen Tillbehör"
})

DarkRP.createEntity("Harris Bipod", {
	ent = "fas2_att_harrisbipod",
    model = "models/weapons/w_ammobox_thrown.mdl",
	price = 99,
	max = 3,
	cmd = "harrisbipod",
	category = "Vapen Tillbehör"
})

DarkRP.createEntity("Leopold MK4", {
	ent = "fas2_att_leupold",
    model = "models/weapons/w_ammobox_thrown.mdl",
	price = 99,
	max = 3,
	cmd = "leopoldmk4",
	category = "Vapen Tillbehör"
})

DarkRP.createEntity("M21 20RND Mag", {
	ent = "fas2_att_m2120mag",
    model = "models/weapons/w_ammobox_thrown.mdl",
	price = 99,
	max = 3,
	cmd = "m2120rnd",
	category = "Vapen Tillbehör"
})

DarkRP.createEntity("PSO-1", {
	ent = "fas2_att_pso1",
    model = "models/weapons/w_ammobox_thrown.mdl",
	price = 99,
	max = 3,
	cmd = "pso1",
	category = "Vapen Tillbehör"
})

DarkRP.createEntity("SG55X 30RND Mag", {
	ent = "fas2_att_sg55x30mag",
    model = "models/weapons/w_ammobox_thrown.mdl",
	price = 99,
	max = 3,
	cmd = "sg55x30rnd",
	category = "Vapen Tillbehör"
})

DarkRP.createEntity("SKS 20RND Mag", {
	ent = "fas2_att_sks20mag",
    model = "models/weapons/w_ammobox_thrown.mdl",
	price = 99,
	max = 3,
	cmd = "sks20rnd",
	category = "Vapen Tillbehör"
})

DarkRP.createEntity("SKS 30RND Mag", {
	ent = "fas2_att_sks30mag",
    model = "models/weapons/w_ammobox_thrown.mdl",
	price = 99,
	max = 3,
	cmd = "sks30rnd",
	category = "Vapen Tillbehör"
})

DarkRP.createEntity("Suppressor", {
	ent = "fas2_att_suppressor",
    model = "models/weapons/w_ammobox_thrown.mdl",
	price = 99,
	max = 3,
	cmd = "suppressor",
	category = "Vapen Tillbehör"
})

DarkRP.createEntity("Tritium Sights", {
	ent = "fas2_att_tritiumsights",
    model = "models/weapons/w_ammobox_thrown.mdl",
	price = 99,
	max = 3,
	cmd = "tritiumsights",
	category = "Vapen Tillbehör"
})

DarkRP.createEntity("UZI Wooden Stock", {
	ent = "fas2_att_uziwoodenstock",
    model = "models/weapons/w_ammobox_thrown.mdl",
	price = 99,
	max = 3,
	cmd = "uziwoodenstock",
	category = "Vapen Tillbehör"
})
