-----------------------------------------------------------------
-- @package     Vliss
-- @authors     Richard
-- @build       v1.4.0
-- @release     12.29.2015
-----------------------------------------------------------------

-----------------------------------------------------------------
-- [ TABLES ]
-----------------------------------------------------------------
Vliss = Vliss or {}
Vliss.Script = Vliss.Script or {}
Vliss.Script.Name = "Vliss"
Vliss.Script.Author = "Richard"
Vliss.Script.Build = "1.4.0"
Vliss.Script.Released = "Dec 29, 2015"
Vliss.Script.Website = "https://scriptfodder.com/scripts/view/1661"
Vliss.Script.UpdateCheck = "http://galileomanager.com/api/products/1661/VersionCheck/v001/index.php?type=json"
Vliss.Script.Documentation = "http://archegamingstudios.net/products/vliss/docs/"

-----------------------------------------------------------------
-- [ TABLES - > GAMEMODES ]
-----------------------------------------------------------------
Vliss.Core = Vliss.Core or {}
Vliss.Commands = Vliss.Commands or {}
Vliss.Messages = Vliss.Messages or {}
Vliss.Language = Vliss.Language or {}
Vliss.Sandbox = Vliss.Sandbox or {}
Vliss.Murder = Vliss.Murder or {}
Vliss.TTT = Vliss.TTT or {}
Vliss.DarkRP = Vliss.DarkRP or {}
Vliss.PropHunt = Vliss.PropHunt or {}
Vliss.ZombieSurvival = Vliss.ZombieSurvival or {}
-----------------------------------------------------------------
-- [ AUTOLOADER ]
-----------------------------------------------------------------

local luaroot = "vliss"
local name = "Vliss"

local VlissStartupHeader = {
    '\n\n',
    [[.................................................................... ]],
}

local VlissStartupInfo = {
    [[[title]....... ]] .. Vliss.Script.Name .. [[ ]],
    [[[build]....... v]] .. Vliss.Script.Build .. [[ ]],
    [[[released].... ]] .. Vliss.Script.Released .. [[ ]],
    [[[author]...... ]] .. Vliss.Script.Author .. [[ ]],
    [[[website]..... ]] .. Vliss.Script.Website .. [[ ]],
}

local VlissStartupFooter = {
    [[.................................................................... ]],
}

function Vliss:PerformCheck(func)
    if (type(func)=="function") then
        return true
    end
    
    return false
end

function game.GetIP()
    local hostip = GetConVarString( "hostip" )
    hostip = tonumber( hostip )
    local ip = {}
    ip[ 1 ] = bit.rshift( bit.band( hostip, 0xFF000000 ), 24 )
    ip[ 2 ] = bit.rshift( bit.band( hostip, 0x00FF0000 ), 16 )
    ip[ 3 ] = bit.rshift( bit.band( hostip, 0x0000FF00 ), 8 )
    ip[ 4 ] = bit.band( hostip, 0x000000FF )
    return table.concat( ip, "." )
end

-----------------------------------------------------------------
-- Trying to leak it? Why don't you go get a real job
-- instead of me paying my taxes on supporting you eating
-- and playing with yourself on the internet.
-----------------------------------------------------------------



for k, i in ipairs( VlissStartupHeader ) do 
    MsgC( Color( 255, 255, 0 ), i .. '\n' )
end

for k, i in ipairs( VlissStartupInfo ) do 
    MsgC( Color( 255, 255, 255 ), i .. '\n' )
end

for k, i in ipairs( VlissStartupFooter ) do 
    MsgC( Color( 255, 255, 0 ), i .. '\n\n' )
end

-----------------------------------------------------------------
-- [ SERVER-SIDE ACTIONS ]
-----------------------------------------------------------------

if SERVER then

    local fol = luaroot .. "/"
    local files, folders = file.Find(fol .. "*", "LUA")

    for k, v in pairs(files) do
        include(fol .. v)
    end

    for _, folder in SortedPairs(folders, true) do
        if folder == "." or folder == ".." then continue end

        for _, File in SortedPairs(file.Find(fol .. folder .. "/sh_*.lua", "LUA"), true) do
            MsgC(Color(255, 255, 0), "[" .. Vliss.Script.Name .. "] SHARED file: " .. File .. "\n")
            AddCSLuaFile(fol .. folder .. "/" .. File)
            include(fol .. folder .. "/" .. File)
        end
    end

    for _, folder in SortedPairs(folders, true) do
        if folder == "." or folder == ".." then continue end

        for _, File in SortedPairs(file.Find(fol .. folder .. "/sv_*.lua", "LUA"), true) do
            MsgC(Color(255, 255, 0), "[" .. Vliss.Script.Name .. "] SERVER file: " .. File .. "\n")
            include(fol .. folder .. "/" .. File)
        end
    end

    for _, folder in SortedPairs(folders, true) do
        if folder == "." or folder == ".." then continue end

        for _, File in SortedPairs(file.Find(fol .. folder .. "/cl_*.lua", "LUA"), true) do
            MsgC(Color(255, 255, 0), "[" .. Vliss.Script.Name .. "] CLIENT file: " .. File .. "\n")
            AddCSLuaFile(fol .. folder .. "/" .. File)
        end
    end

    for _, folder in SortedPairs(folders, true) do
        if folder == "." or folder == ".." then continue end

        for _, File in SortedPairs(file.Find(fol .. folder .. "/vgui_*.lua", "LUA"), true) do
            MsgC(Color(255, 255, 0), "[" .. Vliss.Script.Name .. "] CLIENT file: " .. File .. "\n")
            AddCSLuaFile(fol .. folder .. "/" .. File)
        end
    end

    MsgC(Color( 0, 255, 0 ), "\n..........................[ Vliss Loaded ]..........................\n\n")

end

-----------------------------------------------------------------
-- [ CLIENT-SIDE ACTIONS ]
-----------------------------------------------------------------

if CLIENT then

    local root = "vliss" .. "/"
    local _, folders = file.Find(root .. "*", "LUA")

    for _, folder in SortedPairs(folders, true) do
        if folder == "." or folder == ".." then continue end

        for _, File in SortedPairs(file.Find(root .. folder .. "/sh_*.lua", "LUA"), true) do
            MsgC(Color(255, 255, 0), "[" .. Vliss.Script.Name .. "] SHARED file: " .. File .. "\n")
            include(root .. folder .. "/" .. File)
        end
    end

    for _, folder in SortedPairs(folders, true) do
        for _, File in SortedPairs(file.Find(root .. folder .. "/cl_*.lua", "LUA"), true) do
            MsgC(Color(255, 255, 0), "[" .. Vliss.Script.Name .. "] CLIENT file: " .. File .. "\n")
            include(root .. folder .. "/" .. File)
        end
    end

    for _, folder in SortedPairs(folders, true) do
        for _, File in SortedPairs(file.Find(root .. folder .. "/vgui_*.lua", "LUA"), true) do
            MsgC(Color(255, 0, 0), "[" .. Vliss.Script.Name .. "] VGUI file: " .. File .. "\n")
            include(root .. folder .. "/" .. File)
        end
    end

    MsgC(Color( 0, 255, 0 ), "\n..........................[ Vliss Loaded ]..........................\n\n")

end

-----------------------------------------------------------------
-- [ GAMEMODE CHECKS ]
-----------------------------------------------------------------

hook.Add("PostGamemodeLoaded", "VlissDetectConflicts", function()

    local GMFolder_Sandbox = "sandbox"
    local GMFolder_Murder = "murder"
    local GMFolder_TTT = "terrortown"
    local GMFolder_Darkrp = "darkrp"
    local GMFolder_PropHunt = "prop_hunt"
    local GMFolder_Zombie = "zombiesurvival"

    local VlissAddonUTime
    if Utime then VlissAddonUTime = "Enabled" else VlissAddonUTime = "Disabled" end

    local VlissAddonFAdmin
    if FAdmin then VlissAddonFAdmin = "Enabled" else VlissAddonFAdmin = "Disabled" end

    MsgC(Color( 255, 255, 0 ), "\n.........................[ Addon Checker ]..........................\n\n")
    MsgC(Color( 255, 255, 0 ), "Vliss will now check to see if you have any additional scripts \n")
    MsgC(Color( 255, 255, 0 ), "that can enhance your experience.\n")
    MsgC(Color( 255, 255, 0 ), "....................................................................\n\n")
    MsgC(Color( 255, 255, 255 ), "  UTime...............................[" .. VlissAddonUTime .. "]\n")
    MsgC(Color( 255, 255, 255 ), "  FAdmin..............................[" .. VlissAddonFAdmin .. "]\n")

    MsgC(Color( 255, 255, 0 ), "\n.......................[ Vliss Settings Check ].....................\n\n")
    MsgC(Color( 255, 255, 0 ), "Vliss will now check to see if certain settings should be \n")
    MsgC(Color( 255, 255, 0 ), "re-configured while your gamemode is running.\n\n")

    if GAMEMODE.FolderName == GMFolder_Sandbox then

        MsgC(Color( 255, 255, 255 ), "Gamemode Detected.....................Sandbox \n") 
        MsgC(Color( 255, 255, 255 ), "   Vliss.Sandbox.Enabled..............[ON] \n")
        Vliss.Sandbox.Enabled = true

        MsgC(Color( 255, 255, 255 ), "   Vliss.TTT.Enabled..................[OFF] \n")
        Vliss.TTT.Enabled = false

        MsgC(Color( 255, 255, 255 ), "   Vliss.Murder.Enabled...............[OFF] \n")
        Vliss.Murder.Enabled = false

        MsgC(Color( 255, 255, 255 ), "   Vliss.DarkRP.Enabled...............[OFF] \n")
        Vliss.DarkRP.Enabled = false

        MsgC(Color( 255, 255, 255 ), "   Vliss.PropHunt.Enabled.............[OFF] \n")
        Vliss.PropHunt.Enabled = false

        MsgC(Color( 255, 255, 255 ), "   Vliss.ZombieSurvival.Enabled.......[OFF] \n")
        Vliss.ZombieSurvival.Enabled = false

    elseif GAMEMODE.FolderName == GMFolder_Murder then

        MsgC(Color( 255, 255, 255 ), "Gamemode Detected.....................Murder \n") 
        MsgC(Color( 255, 255, 255 ), "   Vliss.Sandbox.Enabled..............[OFF] \n")
        Vliss.Sandbox.Enabled = false

        MsgC(Color( 255, 255, 255 ), "   Vliss.TTT.Enabled..................[OFF] \n")
        Vliss.TTT.Enabled = false

        MsgC(Color( 255, 255, 255 ), "   Vliss.Murder.Enabled...............[ON] \n")
        Vliss.Murder.Enabled = true

        MsgC(Color( 255, 255, 255 ), "   Vliss.DarkRP.Enabled...............[OFF] \n")
        Vliss.DarkRP.Enabled = false

        MsgC(Color( 255, 255, 255 ), "   Vliss.PropHunt.Enabled.............[OFF] \n")
        Vliss.PropHunt.Enabled = false

        MsgC(Color( 255, 255, 255 ), "   Vliss.Core.TeamMode................[OFF] \n")
        Vliss.Core.TeamMode = false

        MsgC(Color( 255, 255, 255 ), "   Vliss.Core.UserGroupColoring.......[OFF] \n\n")
        Vliss.Core.UserGroupColoring = false

        MsgC(Color( 255, 255, 255 ), "   Vliss.ZombieSurvival.Enabled.......[OFF] \n")
        Vliss.ZombieSurvival.Enabled = false

    elseif GAMEMODE.FolderName == GMFolder_TTT then

        MsgC(Color( 255, 255, 255 ), "Gamemode Detected.....................Terroist Town \n") 
        MsgC(Color( 255, 255, 255 ), "   Vliss.Sandbox.Enabled..............[OFF] \n")
        Vliss.Sandbox.Enabled = false

        MsgC(Color( 255, 255, 255 ), "   Vliss.TTT.Enabled..................[ON] \n")
        Vliss.TTT.Enabled = true

        MsgC(Color( 255, 255, 255 ), "   Vliss.Murder.Enabled...............[OFF] \n")
        Vliss.Murder.Enabled = false

        MsgC(Color( 255, 255, 255 ), "   Vliss.DarkRP.Enabled...............[OFF] \n")
        Vliss.DarkRP.Enabled = false

        MsgC(Color( 255, 255, 255 ), "   Vliss.PropHunt.Enabled.............[OFF] \n")
        Vliss.PropHunt.Enabled = false

        MsgC(Color( 255, 255, 255 ), "   Vliss.ZombieSurvival.Enabled.......[OFF] \n")
        Vliss.ZombieSurvival.Enabled = false

    elseif GAMEMODE.FolderName == GMFolder_Darkrp then

        MsgC(Color( 255, 255, 255 ), "Gamemode Detected.....................DarkRP \n") 
        MsgC(Color( 255, 255, 255 ), "   Vliss.Sandbox.Enabled..............[OFF] \n")
        Vliss.Sandbox.Enabled = false

        MsgC(Color( 255, 255, 255 ), "   Vliss.TTT.Enabled..................[OFF] \n")
        Vliss.TTT.Enabled = false

        MsgC(Color( 255, 255, 255 ), "   Vliss.Murder.Enabled...............[OFF] \n")
        Vliss.Murder.Enabled = false

        MsgC(Color( 255, 255, 255 ), "   Vliss.DarkRP.Enabled...............[ON] \n")
        Vliss.DarkRP.Enabled = true

        MsgC(Color( 255, 255, 255 ), "   Vliss.PropHunt.Enabled.............[OFF] \n")
        Vliss.PropHunt.Enabled = false

        MsgC(Color( 255, 255, 255 ), "   Vliss.ZombieSurvival.Enabled.......[OFF] \n")
        Vliss.ZombieSurvival.Enabled = false

    elseif GAMEMODE.FolderName == GMFolder_PropHunt then

        MsgC(Color( 255, 255, 255 ), "Gamemode Detected.....................PropHunt \n") 
        MsgC(Color( 255, 255, 255 ), "   Vliss.Sandbox.Enabled..............[OFF] \n")
        Vliss.Sandbox.Enabled = false

        MsgC(Color( 255, 255, 255 ), "   Vliss.TTT.Enabled..................[OFF] \n")
        Vliss.TTT.Enabled = false

        MsgC(Color( 255, 255, 255 ), "   Vliss.Murder.Enabled...............[OFF] \n")
        Vliss.Murder.Enabled = false

        MsgC(Color( 255, 255, 255 ), "   Vliss.DarkRP.Enabled...............[OFF] \n")
        Vliss.DarkRP.Enabled = false

        MsgC(Color( 255, 255, 255 ), "   Vliss.PropHunt.Enabled.............[ON] \n")
        Vliss.PropHunt.Enabled = true

        MsgC(Color( 255, 255, 255 ), "   Vliss.ZombieSurvival.Enabled.......[OFF] \n")
        Vliss.ZombieSurvival.Enabled = false

    elseif GAMEMODE.FolderName == GMFolder_Zombie then

        MsgC(Color( 255, 255, 255 ), "Gamemode Detected.....................Zombie Survival \n") 
        MsgC(Color( 255, 255, 255 ), "   Vliss.Sandbox.Enabled..............[OFF] \n")
        Vliss.Sandbox.Enabled = false

        MsgC(Color( 255, 255, 255 ), "   Vliss.TTT.Enabled..................[OFF] \n")
        Vliss.TTT.Enabled = false

        MsgC(Color( 255, 255, 255 ), "   Vliss.Murder.Enabled...............[OFF] \n")
        Vliss.Murder.Enabled = false

        MsgC(Color( 255, 255, 255 ), "   Vliss.DarkRP.Enabled...............[OFF] \n")
        Vliss.DarkRP.Enabled = false

        MsgC(Color( 255, 255, 255 ), "   Vliss.PropHunt.Enabled.............[OFF] \n")
        Vliss.PropHunt.Enabled = false

        MsgC(Color( 255, 255, 255 ), "   Vliss.ZombieSurvival.Enabled.......[ON] \n")
        Vliss.ZombieSurvival.Enabled = true

    else

        MsgC(Color( 255, 255, 255 ), "Gamemode Detected.....................Unknown \n")
        MsgC(Color( 255, 255, 0 ), "I don't seem to know what gamemode you're playing. I am only as \n")
        MsgC(Color( 255, 255, 0 ), "good as the developer, so let him know! This won't break \n")
        MsgC(Color( 255, 255, 0 ), "anything - I just can't make sure you've set things properly. \n\n")

    end

    MsgC(Color( 255, 255, 0 ), ".............................[ Complete ]...........................\n\n")

    MsgC(Color( 0, 255, 0 ), "\n.......................[ Vliss Load Complete ]......................\n\n")
    MsgC(Color( 0, 255, 0 ), "Vliss has completed loading everything. If you experience any \n")
    MsgC(Color( 0, 255, 0 ), "issues please make sure to contact the script author.\n")
    MsgC(Color( 0, 255, 0 ), "....................................................................\n\n")

end)

