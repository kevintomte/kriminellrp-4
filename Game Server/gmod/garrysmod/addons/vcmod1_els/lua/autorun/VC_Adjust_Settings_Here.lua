// Copyright © 2012-2015 VCMod (freemmaann). All Rights Reserved. if you have any complaints or ideas contact me: email - freemmaann@gmail.com or skype - comman6.

// Set this to "= true" if you want only SuperAdmins being able to adjust administrator options.
VC_Only_Allow_Admin_Settings_For_SuperAdmins = false