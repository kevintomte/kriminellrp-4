Liko = Liko or {}
Liko.Settings = Liko.Settings or {}
Liko.Language = Liko.Language or {}

Liko.Settings.AnnouncementEnabled = true
Liko.Settings.AnnouncementText = 
[[
Välkommen till KriminellRP!










]]

Liko.Settings.EscapeScreenToggle = true
Liko.Settings.KeybindAllowToggle = true
Liko.Settings.KeybindEnum = KEY_F7

Liko.Settings.NetworkName = "KriminellRP"
Liko.Settings.NetworkNameColor = Color( 255, 255, 255, 255 )
Liko.Settings.BottomBarColor = Color( 255, 255, 255, 170 )

Liko.Settings.BackgroundsEnable = true
Liko.Settings.Backgrounds = {
	"http://178.32.177.208/gmod/images/backgrounds/1.jpg",
	"http://178.32.177.208/gmod/images/backgrounds/2.jpg",
	"http://178.32.177.208/gmod/images/backgrounds/3.jpg",
	"http://178.32.177.208/gmod/images/backgrounds/4.jpg",
	"http://178.32.177.208/gmod/images/backgrounds/5.jpg",
	"http://178.32.177.208/gmod/images/backgrounds/6.jpg",
	"http://178.32.177.208/gmod/images/backgrounds/7.jpg"
}

Liko.Settings.AdvertisementEnable = false
Liko.Settings.AdvertisementYouTubeEnabled = false
Liko.Settings.AdvertisementsYouTube = { "" }
Liko.Settings.AdvertisementWebmAutoplay = false
Liko.Settings.AdvertisementWebmStartvol = 0.5
Liko.Settings.AdvertisementsWebm = { "" }

Liko.Settings.ClockEnabled = false
Liko.Settings.ClockTextColor = Color ( 204, 132, 37, 255 )



-----------------------------------------------------------------

--  Menu Buttons

-----------------------------------------------------------------



-----------------------------------------------------------------

-- Button Properties

--      name = (text to display on button)

--      description = (description under name of button)

--      icon = (icon to use for button)

--      buttonNormal = (color of button in regular state)

--      buttonHover = (color of button when mouse hovers)

--      textNormal = (color of text in regular state)

--      textHover = (color of text when mouse hovers)

--      enabled = (if button should show or not)

--      func = (what to do when button is pressed)

--

--

-- To toggle buttons to do what you want, you will change the

-- code after each 'func = function()' line.

--

-- To open a URL in the STEAM BROWSER

--      gui.OpenURL( "http://urlhere.com"

--

-- To open a URL with the BUILT IN BROWSER 

--       Liko:OpenURL( "http://urlhere.com", "Titlebar Name" )

--

-- To open a Frame with JUST text (IE: Rules)

--      Liko:OpenText( "Text to display", "Titlebar Name" )

--

-- To run a ConCommand:

--      RunConsoleCommand('concommand_here')

--

-----------------------------------------------------------------



Liko.Settings.MenuLinkDonate = "http://oblivionnation.nn.pe/index.php?topic=198.0"
Liko.Settings.MenuLinkWebsite = "http://oblivionnation.nn.pe"
Liko.Settings.MenuLinkWorkshop = "http://steamcommunity.com/groups/oblivionnation"

Liko.Settings.Buttons = {
	{
		name = "ÅTERUPPTA",
		description = "GÅ TILLBAKA TILL SPELET",
		icon = "liko/liko_btn_resume.png",
		buttonNormal = Color( 47, 82, 57, 190 ),
		buttonHover = Color( 47, 82, 57, 240 ),
		textNormal = Color( 255, 255, 255, 255 ),
		textHover = Color( 255, 255, 255, 255 ),
		enabled = true,
		func = function() LikoPanelMenu:SetVisible( false ) end
	},
	{
		name = "DONERA",
		description = "VARJE SLANT HJÄLPER :)",
		icon = "liko/liko_btn_donate.png",
		buttonNormal = Color( 64, 105, 126, 190 ),
		buttonHover = Color( 64, 105, 126, 240 ),
		textNormal = Color( 255, 255, 255, 255 ),
		textHover = Color( 255, 255, 255, 255 ),
		enabled = true,
		func = function() Liko:OpenURL( Liko.Settings.MenuLinkDonate, "Donera till KriminelRP!" ) end
	},
	{
		name = "HEMSIDA",
		description = "BESÖK VÅR HEMSIDA",
		icon = "liko/liko_btn_website.png",
		buttonNormal = Color( 163, 135, 79, 190 ),
		buttonHover = Color( 163, 135, 79, 240 ),
		textNormal = Color( 255, 255, 255, 255 ),
		textHover = Color( 255, 255, 255, 255 ),
		enabled = true,
		func = function() Liko:OpenURL( Liko.Settings.MenuLinkWebsite, "KriminelRP" ) end
	},
	{
		name = "LOGGA UT",
		description = "LOGGA UT FRÅN SERVERN",
		icon = "liko/liko_btn_disconnect.png",
		buttonNormal = Color( 124, 51, 50, 190 ),
		buttonHover = Color( 124, 51, 50, 240 ),
		textNormal = Color( 255, 255, 255, 255 ),
		textHover = Color( 255, 255, 255, 255 ),
		enabled = true,
		func = function() RunConsoleCommand( "disconnect" ) end
	},
	{
		name = "MAIN MENU",
		description = "FÖR INSTÄLLNINGAR OCH MER",
		icon = "liko/liko_btn_mainmenu.png",
		buttonNormal = Color( 66, 67, 118, 190 ),
		buttonHover = Color( 66, 67, 118, 240 ),
		textNormal = Color( 255, 255, 255, 255 ),
		textHover = Color( 255, 255, 255, 255 ),
		enabled = true,
		func = function() LikoPanelMenu:SetVisible( false ) gui.ActivateGameUI() end
	},
}

Liko.Settings.ServersEnabled = true
Liko.Settings.UseServerIconsWithText = true
Liko.Settings.Servers = {
	{
		hostname = "DU SPELAR PÅ KRIMINELLRP || 178.32.177.208",
		icon = "liko/liko_btn_server.png",
		ip = ""
	}
}

Liko.Settings.ServerButtonColor = Color( 15, 15, 15, 0 )
Liko.Settings.ServerButtonHoverColor = Color( 255, 255, 255, 220 )
Liko.Settings.ServerButtonTextColor = Color( 255, 255, 255, 255 )
Liko.Settings.ServerButtonHoverTextColor = Color( 0, 0, 0, 255 )

Liko.Settings.BrowserColor = Color( 0, 0, 0, 240 )
Liko.BCColorServer = Color( 255, 255, 0 )
Liko.BCColorName = Color( 77, 145, 255 )
Liko.BCColorMsg = Color( 255, 255, 255 )
Liko.BCColorValue = Color( 255, 0, 0 )
Liko.BCColorValue2 = Color( 255, 166, 0 )
Liko.BCColorBind = Color( 255, 255, 0 )
