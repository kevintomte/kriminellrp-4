local function IsHere(targ)
	for k, v in pairs(player.GetAll()) do 
		if v:Nick() == targ then 
			return true
		end
	end
	return false
end
local function FindPlayer( targ )
	local target
	for k, v in pairs( player.GetAll() ) do
		if targ == v:Name() then
			target = v
		end
	end
	return target
end

hook.Add( "ARS_PlayerReported", "There has been a report", function(reporter, name, reason)
	if ARS.SendChatNotification then 
		for k, v in pairs(player.GetAll()) do
			if v:IsARSAdmin() then 
				v:ChatPrint( reporter.." has reported "..name.." for "..reason.."." )
			end
		end
	end
end)

hook.Add( "ARS_AdminClaimed_Report", "Send notification to reporter", function(number, ply)
	local FILE = file.Read( "ars_reports/savedreports.txt", "DATA" )
	local TABLE = util.JSONToTable(FILE)
	local v = CheckTable(TABLE, number)
	if IsHere(v.REPORTER) then
		if ARS.SendTypeOfNotification == "Both" then 
			ARSNotify(FindPlayer(v.REPORTER), 1, 10, ply.." has claimed your report and should be with you shortly.")
			FindPlayer(v.REPORTER):ChatPrint( ply.." has claimed your report and should be with you shortly." )
		elseif ARS.SendTypeOfNotification == "Chat" then
			FindPlayer(v.REPORTER):ChatPrint( ply.." has claimed your report and should be with you shortly." )
		elseif ARS.SendTypeOfNotification == "Notification" then
			ARSNotify(FindPlayer(v.REPORTER), 1, 10, ply.." has claimed your report and should be with you shortly.")
		end
	end
end)

