"TableToKeyValues"
{
	"1"
	{
		"name"		"Indianapolis Metro Police Dispatch"
		"url"		"http://198.178.123.14:8424"
	}
	"2"
	{
		"name"		"Lincoln Nebraska EDACS"
		"url"		"http://108.167.16.109:8000"
	}
	"3"
	{
		"name"		"Coral Springs Florida Police and Fire Dispatch"
		"url"		"http://173.246.43.210:8000"
	}
	"4"
	{
		"name"		"Chicago Police Department"
		"url"		"http://relay.broadcastify.com:80/il_chicago_police2.mp3"
	}
	"5"
	{
		"name"		"Stockholm / Västerås Airport"
		"url"		"http://relay.broadcastify.com:80/267763632"
	}
	"6"
	{
		"name"		"Stockholm / Västerås Airport"
		"url"		"http://relay.broadcastify.com:80/c8h01qnzsm5d"
	}
	"7"
	{
		"name"		"Swedish West Coast"
		"url"		"http://relay.broadcastify.com:80/517704592"
	}
	"8"
	{
		"name"		"Trollhättan och Stäve Tower, Sweden"
		"url"		"http://relay.broadcastify.com:80/920210032"
	}
}